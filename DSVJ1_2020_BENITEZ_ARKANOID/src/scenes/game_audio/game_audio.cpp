#include "game_audio.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace game_audio
	{
		static Color musicMute = BLACK;
		static Color musicLow = BLACK;
		static Color musicHigh = WHITE;
		static Color soundsMute = BLACK;
		static Color soundsLow = BLACK;
		static Color soundsHigh = WHITE;

		static Color quitButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX, pos::audioRecPosY, pos::audioRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					musicMute = WHITE;
					musicLow = BLACK;
					musicHigh = BLACK;
					audio::musicVolume = 0.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX2, pos::audioRecPosY, pos::audioRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					musicMute = BLACK;
					musicLow = WHITE;
					musicHigh = BLACK;
					audio::musicVolume = 0.2f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX3, pos::audioRecPosY, pos::audioRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					musicMute = BLACK;
					musicLow = BLACK;
					musicHigh = WHITE;
					audio::musicVolume = 1.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX4, pos::audioRecPosY2, pos::audioRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					soundsMute = WHITE;
					soundsLow = BLACK;
					soundsHigh = BLACK;
					audio::soundVolume = 0.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX5, pos::audioRecPosY2, pos::audioRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					soundsMute = BLACK;
					soundsLow = WHITE;
					soundsHigh = BLACK;
					audio::soundVolume = 0.2f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX6, pos::audioRecPosY2, pos::audioRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					soundsMute = BLACK;
					soundsLow = BLACK;
					soundsHigh = WHITE;
					audio::soundVolume = 1.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::centerDownButtonPosX, pos::downButtonsPosY, (float)textures::quit.width, (float)textures::quit.height }))
			{
				quitButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::previousScene;
				}
			}
			else
			{
				quitButtonStatus = WHITE;
			}

			if (pong::game::previousScene == pong::game::Scene::GAMEPLAY)
			{
				PlayMusicStream(audio::gameMusic.gameplay);
				audio::gameplayAudio();
				audio::setSoundsVolume();
				audio::setMusicVolume();
				sounds.button = false;
			}
			else
			{
				if (pong::game::previousScene != pong::game::Scene::RESULT)
				{
					PlayMusicStream(audio::gameMusic.menu);
					audio::menuAudio();
				}
				audio::setSoundsVolume();
				audio::setMusicVolume();				
				sounds.button = false;
			}
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);

			DrawText("Audio", pos::audioPosX, pos::audioPosY, pos::textsSize5, BLACK);
			DrawText("Audio", pos::audioPosX2, pos::audioPosY2, pos::textsSize5, WHITE);

			DrawText("Music", pos::audioPosX3, pos::audioPosY3, pos::textsSize, BLACK);
			DrawText("Mute", pos::audioPosX4, pos::audioPosY4, pos::textsSize, musicMute);
			DrawText("Low", pos::audioPosX5, pos::audioPosY4, pos::textsSize, musicLow);
			DrawText("High", pos::audioPosX6, pos::audioPosY4, pos::textsSize, musicHigh);
			DrawRectangleLinesEx({ pos::audioRecPosX, pos::audioRecPosY, pos::audioRecWidth , pos::recHeight }, pos::recSizeLines, musicMute);
			DrawRectangleLinesEx({ pos::audioRecPosX2, pos::audioRecPosY, pos::audioRecWidth , pos::recHeight }, pos::recSizeLines, musicLow);
			DrawRectangleLinesEx({ pos::audioRecPosX3, pos::audioRecPosY, pos::audioRecWidth , pos::recHeight }, pos::recSizeLines, musicHigh);

			DrawText("Sounds", pos::audioPosX7, pos::audioPosY5, pos::textsSize, BLACK);
			DrawText("Mute", pos::audioPosX8, pos::audioPosY6, pos::textsSize, soundsMute);
			DrawText("Low", pos::audioPosX9, pos::audioPosY6, pos::textsSize, soundsLow);
			DrawText("High", pos::audioPosX10, pos::audioPosY6, pos::textsSize, soundsHigh);
			DrawRectangleLinesEx({ pos::audioRecPosX4, pos::audioRecPosY2, pos::audioRecWidth , pos::recHeight }, pos::recSizeLines, soundsMute);
			DrawRectangleLinesEx({ pos::audioRecPosX5, pos::audioRecPosY2, pos::audioRecWidth , pos::recHeight }, pos::recSizeLines, soundsLow);
			DrawRectangleLinesEx({ pos::audioRecPosX6, pos::audioRecPosY2, pos::audioRecWidth , pos::recHeight }, pos::recSizeLines, soundsHigh);

			DrawTexture(textures::quit, (int)pos::centerDownButtonPosX, (int)pos::downButtonsPosY, quitButtonStatus);
		}

		void deinit()
		{
		}
	}
}