#include "resolutions.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace resolutions
	{
		static Color screenWidth900 = BLACK;
		static Color screenWidth1200 = WHITE;
		static Color screenWidth1500 = BLACK;
		static Color screenHeight800 = BLACK;
		static Color screenHeight900 = WHITE;
		static Color screenHeight1000 = BLACK;
		
		static Color quitButtonStatus = WHITE;

		void init()
		{
			textures::quit = LoadTexture("res/raw/textures/quit.png");
			textures::quit.width = sizes::sizeCircleButtons;
			textures::quit.height = sizes::sizeCircleButtons;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX, pos::resolutionsRecPosY, pos::resolutionsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					screenWidth900 = WHITE;
					screenWidth1200 = BLACK;
					screenWidth1500 = BLACK;
					screenWidth = 900;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX2, pos::resolutionsRecPosY, pos::resolutionsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					screenWidth900 = BLACK;
					screenWidth1200 = WHITE;
					screenWidth1500 = BLACK;
					screenWidth = 1200;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX3, pos::resolutionsRecPosY, pos::resolutionsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					screenWidth900 = BLACK;
					screenWidth1200 = BLACK;
					screenWidth1500 = WHITE;
					screenWidth = 1500;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX4, pos::resolutionsRecPosY2, pos::resolutionsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					screenHeight800 = WHITE;
					screenHeight900 = BLACK;
					screenHeight1000 = BLACK;
					screenHeight = 800;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX5, pos::resolutionsRecPosY2, pos::resolutionsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					screenHeight800 = BLACK;
					screenHeight900 = WHITE;
					screenHeight1000 = BLACK;
					screenHeight = 900;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX6, pos::resolutionsRecPosY2, pos::resolutionsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					screenHeight800 = BLACK;
					screenHeight900 = BLACK;
					screenHeight1000 = WHITE;
					screenHeight = 1000;
				}
			}	
			else if (CheckCollisionPointRec(mousePoint, { pos::centerDownButtonPosX, pos::downButtonsPosY, (float)textures::quit.width, (float)textures::quit.height }))
			{
				quitButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::previousScene;
				}
			}
			else
			{
				quitButtonStatus = WHITE;
			}

			pos::updateVariables();
			sizes::updateSizes();

			textures::backgroundGameplay.width = (int)screenWidth;
			textures::backgroundBorder.width = (int)screenWidth;
			textures::backgroundMenu.width = (int)screenWidth;

			textures::backgroundGameplay.height = (int)screenHeight;			
			textures::backgroundBorder.height = (int)screenHeight;			
			textures::backgroundMenu.height = (int)screenHeight;

			textures::blocksTexture.width = (int)sizes::blocksSizeX;
			textures::unbreakableBlocks.width = (int)sizes::blocksSizeX;
			textures::brokenBlocks.width = (int)sizes::blocksSizeX;
			textures::brokenBlocks2.width = (int)sizes::blocksSizeX;

			if (pong::game::previousScene != pong::game::Scene::RESULT)
			{
				PlayMusicStream(audio::gameMusic.menu);
				audio::menuAudio();
			}
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);

			DrawText("Resolution", pos::resolutionsPosX, pos::resolutionsPosY, pos::textsSize5, BLACK);
			DrawText("Resolution", pos::resolutionsPosX2, pos::resolutionsPosY2, pos::textsSize5, WHITE);

			DrawText("Width", pos::resolutionsPosX3, pos::resolutionsPosY3, pos::textsSize, BLACK);
			DrawText("900", pos::resolutionsPosX4, pos::resolutionsPosY4, pos::textsSize, screenWidth900);
			DrawText("1200", pos::resolutionsPosX5, pos::resolutionsPosY4, pos::textsSize, screenWidth1200);
			DrawText("1500", pos::resolutionsPosX6, pos::resolutionsPosY4, pos::textsSize, screenWidth1500);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX, pos::resolutionsRecPosY, pos::resolutionsRecWidth , pos::recHeight }, pos::recSizeLines, screenWidth900);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX2, pos::resolutionsRecPosY, pos::resolutionsRecWidth , pos::recHeight }, pos::recSizeLines, screenWidth1200);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX3, pos::resolutionsRecPosY, pos::resolutionsRecWidth , pos::recHeight }, pos::recSizeLines, screenWidth1500);

			DrawText("Height", pos::resolutionsPosX7, pos::resolutionsPosY5, pos::textsSize, BLACK);
			DrawText("800", pos::resolutionsPosX8, pos::resolutionsPosY6, pos::textsSize, screenHeight800);
			DrawText("900", pos::resolutionsPosX9, pos::resolutionsPosY6, pos::textsSize, screenHeight900);
			DrawText("1000", pos::resolutionsPosX10, pos::resolutionsPosY6, pos::textsSize, screenHeight1000);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX4, pos::resolutionsRecPosY2, pos::resolutionsRecWidth , pos::recHeight }, pos::recSizeLines, screenHeight800);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX5, pos::resolutionsRecPosY2, pos::resolutionsRecWidth , pos::recHeight }, pos::recSizeLines, screenHeight900);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX6, pos::resolutionsRecPosY2, pos::resolutionsRecWidth , pos::recHeight }, pos::recSizeLines, screenHeight1000);

			DrawTexture(textures::quit, (int)pos::centerDownButtonPosX, (int)pos::downButtonsPosY, quitButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::quit);
		}
	}
}
