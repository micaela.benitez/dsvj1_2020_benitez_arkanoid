#ifndef PADDLE_COLOR_H
#define PADDLE_COLOR_H

#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace paddle_color
	{
		enum class PaddleColor { cVIOLET, cBLUE, cGREEN, cRED, cPINK, cORANGE, cYELLOW, cGRAY, cBLACK};

		extern PaddleColor paddleColor;

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif