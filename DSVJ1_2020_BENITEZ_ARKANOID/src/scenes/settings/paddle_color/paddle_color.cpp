#include "paddle_color.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace paddle_color
	{
		PaddleColor paddleColor;
		static int numberColorPaddle = 0;

		static Color leftArrowButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color settingsButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		void init()
		{
			paddleColor = PaddleColor::cBLUE;
			
			// Short paddles
			textures::blueShortPaddleTexture = LoadTexture("res/assets/textures/bluepaddleshort.png");
			textures::blueShortPaddleTexture.width = playerWidthShort;
			textures::blueShortPaddleTexture.height = (int)player.height;

			textures::violetShortPaddleTexture = LoadTexture("res/raw/textures/violetpaddleshort.png");
			textures::violetShortPaddleTexture.width = playerWidthShort;
			textures::violetShortPaddleTexture.height = (int)player.height;

			textures::greenShortPaddleTexture = LoadTexture("res/raw/textures/greenpaddleshort.png");
			textures::greenShortPaddleTexture.width = playerWidthShort;
			textures::greenShortPaddleTexture.height = (int)player.height;

			textures::redShortPaddleTexture = LoadTexture("res/raw/textures/redpaddleshort.png");
			textures::redShortPaddleTexture.width = playerWidthShort;
			textures::redShortPaddleTexture.height = (int)player.height;

			textures::pinkShortPaddleTexture = LoadTexture("res/raw/textures/pinkpaddleshort.png");
			textures::pinkShortPaddleTexture.width = playerWidthShort;
			textures::pinkShortPaddleTexture.height = (int)player.height;

			textures::orangeShortPaddleTexture = LoadTexture("res/raw/textures/orangepaddleshort.png");
			textures::orangeShortPaddleTexture.width = playerWidthShort;
			textures::orangeShortPaddleTexture.height = (int)player.height;

			textures::yellowShortPaddleTexture = LoadTexture("res/raw/textures/yellowpaddleshort.png");
			textures::yellowShortPaddleTexture.width = playerWidthShort;
			textures::yellowShortPaddleTexture.height = (int)player.height;

			textures::grayShortPaddleTexture = LoadTexture("res/raw/textures/graypaddleshort.png");
			textures::grayShortPaddleTexture.width = playerWidthShort;
			textures::grayShortPaddleTexture.height = (int)player.height;

			textures::blackShortPaddleTexture = LoadTexture("res/raw/textures/blackpaddleshort.png");
			textures::blackShortPaddleTexture.width = playerWidthShort;
			textures::blackShortPaddleTexture.height = (int)player.height;

			// Normal paddles
			textures::bluePaddleTexture = LoadTexture("res/assets/textures/bluepaddle.png");
			textures::bluePaddleTexture.width = (int)player.width;
			textures::bluePaddleTexture.height = (int)player.height;

			textures::violetPaddleTexture = LoadTexture("res/raw/textures/violetpaddle.png");
			textures::violetPaddleTexture.width = (int)player.width;
			textures::violetPaddleTexture.height = (int)player.height;

			textures::greenPaddleTexture = LoadTexture("res/raw/textures/greenpaddle.png");
			textures::greenPaddleTexture.width = (int)player.width;
			textures::greenPaddleTexture.height = (int)player.height;

			textures::redPaddleTexture = LoadTexture("res/raw/textures/redpaddle.png");
			textures::redPaddleTexture.width = (int)player.width;
			textures::redPaddleTexture.height = (int)player.height;

			textures::pinkPaddleTexture = LoadTexture("res/raw/textures/pinkpaddle.png");
			textures::pinkPaddleTexture.width = (int)player.width;
			textures::pinkPaddleTexture.height = (int)player.height;

			textures::orangePaddleTexture = LoadTexture("res/raw/textures/orangepaddle.png");
			textures::orangePaddleTexture.width = (int)player.width;
			textures::orangePaddleTexture.height = (int)player.height;

			textures::yellowPaddleTexture = LoadTexture("res/raw/textures/yellowpaddle.png");
			textures::yellowPaddleTexture.width = (int)player.width;
			textures::yellowPaddleTexture.height = (int)player.height;

			textures::grayPaddleTexture = LoadTexture("res/raw/textures/graypaddle.png");
			textures::grayPaddleTexture.width = (int)player.width;
			textures::grayPaddleTexture.height = (int)player.height;

			textures::blackPaddleTexture = LoadTexture("res/raw/textures/blackpaddle.png");
			textures::blackPaddleTexture.width = (int)player.width;
			textures::blackPaddleTexture.height = (int)player.height;

			// Long paddles
			textures::blueLongPaddleTexture = LoadTexture("res/assets/textures/bluepaddlelong.png");
			textures::blueLongPaddleTexture.width = playerWidthLong;
			textures::blueLongPaddleTexture.height = (int)player.height;

			textures::violetLongPaddleTexture = LoadTexture("res/raw/textures/violetpaddlelong.png");
			textures::violetLongPaddleTexture.width = playerWidthLong;
			textures::violetLongPaddleTexture.height = (int)player.height;

			textures::greenLongPaddleTexture = LoadTexture("res/raw/textures/greenpaddlelong.png");
			textures::greenLongPaddleTexture.width = playerWidthLong;
			textures::greenLongPaddleTexture.height = (int)player.height;

			textures::redLongPaddleTexture = LoadTexture("res/raw/textures/redpaddlelong.png");
			textures::redLongPaddleTexture.width = playerWidthLong;
			textures::redLongPaddleTexture.height = (int)player.height;

			textures::pinkLongPaddleTexture = LoadTexture("res/raw/textures/pinkpaddlelong.png");
			textures::pinkLongPaddleTexture.width = playerWidthLong;
			textures::pinkLongPaddleTexture.height = (int)player.height;

			textures::orangeLongPaddleTexture = LoadTexture("res/raw/textures/orangepaddlelong.png");
			textures::orangeLongPaddleTexture.width = playerWidthLong;
			textures::orangeLongPaddleTexture.height = (int)player.height;

			textures::yellowLongPaddleTexture = LoadTexture("res/raw/textures/yellowpaddlelong.png");
			textures::yellowLongPaddleTexture.width = playerWidthLong;
			textures::yellowLongPaddleTexture.height = (int)player.height;

			textures::grayLongPaddleTexture = LoadTexture("res/raw/textures/graypaddlelong.png");
			textures::grayLongPaddleTexture.width = playerWidthLong;
			textures::grayLongPaddleTexture.height = (int)player.height;

			textures::blackLongPaddleTexture = LoadTexture("res/raw/textures/blackpaddlelong.png");
			textures::blackLongPaddleTexture.width = playerWidthLong;
			textures::blackLongPaddleTexture.height = (int)player.height;
		}

		void update()
		{
			mousePoint = GetMousePosition();
			
			if (CheckCollisionPointRec(mousePoint, { pos::leftArrowPosX, pos::leftAndRightArrowPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
			{
				leftArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					numberColorPaddle--;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightArrowPosX,  pos::leftAndRightArrowPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					numberColorPaddle++;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX,  pos::downButtonsPosY, (float)textures::settingsCircleButton.width, (float)textures::settingsCircleButton.height }))
			{
				settingsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::SETTINGS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::PADDLECOLOR;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::PADDLECOLOR;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				leftArrowButtonStatus = WHITE;
				rightArrowButtonStatus = WHITE;
				menuButtonStatus = WHITE;
				settingsButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			if (numberColorPaddle < 0) numberColorPaddle = 8;
			else if (numberColorPaddle > 8) numberColorPaddle = 0;

			if (numberColorPaddle == 0) paddleColor = PaddleColor::cBLUE;
			else if (numberColorPaddle == 1) paddleColor = PaddleColor::cVIOLET;
			else if (numberColorPaddle == 2) paddleColor = PaddleColor::cGREEN;
			else if (numberColorPaddle == 3) paddleColor = PaddleColor::cRED;
			else if (numberColorPaddle == 4) paddleColor = PaddleColor::cPINK;
			else if (numberColorPaddle == 5) paddleColor = PaddleColor::cORANGE;
			else if (numberColorPaddle == 6) paddleColor = PaddleColor::cYELLOW;
			else if (numberColorPaddle == 7) paddleColor = PaddleColor::cGRAY;
			else if (numberColorPaddle == 8) paddleColor = PaddleColor::cBLACK;

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawTexture(textures::leftArrow, (int)pos::leftArrowPosX, (int)pos::leftAndRightArrowPosY, leftArrowButtonStatus);
			DrawTexture(textures::rightArrow, (int)pos::rightArrowPosX, (int)pos::leftAndRightArrowPosY, rightArrowButtonStatus);
			DrawTexture(textures::menuCircleButton, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::settingsCircleButton, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, settingsButtonStatus);

			if (screenWidth >= 1200)
			{
				DrawText("Select a color for the paddle", pos::paddleColorTextPosX, pos::colorTextPosY, pos::textsSize5, BLACK);
				DrawText("Select a color for the paddle", pos::paddleColorTextPosX2, pos::colorTextPosY2, pos::textsSize5, WHITE);
			}
			else
			{
				DrawText("Select a color", pos::paddleColorTextPosX3, pos::colorTextPosY3, pos::textsSize5, BLACK);
				DrawText("Select a color", pos::paddleColorTextPosX4, pos::colorTextPosY4, pos::textsSize5, WHITE);
				DrawText("for the paddle", pos::paddleColorTextPosX5, pos::colorTextPosY5, pos::textsSize5, BLACK);
				DrawText("for the paddle", pos::paddleColorTextPosX6, pos::colorTextPosY6, pos::textsSize5, WHITE);
			}

			DrawTexture(textures::arrows, (int)pos::arrowsPosX, (int)pos::arrowsPosY, WHITE);

			if (paddleColor == PaddleColor::cBLUE) DrawTexture(textures::bluePaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cVIOLET) DrawTexture(textures::violetPaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cGREEN) DrawTexture(textures::greenPaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cRED) DrawTexture(textures::redPaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cPINK) DrawTexture(textures::pinkPaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cORANGE) DrawTexture(textures::orangePaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cYELLOW) DrawTexture(textures::yellowPaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cGRAY) DrawTexture(textures::grayPaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);
			else if (paddleColor == PaddleColor::cBLACK) DrawTexture(textures::blackPaddleTexture, (int)pos::paddlePosX, (int)pos::entitiesPosY, WHITE);

			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
			// Short paddles
			UnloadTexture(textures::blueShortPaddleTexture);
			UnloadTexture(textures::violetShortPaddleTexture);
			UnloadTexture(textures::greenShortPaddleTexture);
			UnloadTexture(textures::redShortPaddleTexture);
			UnloadTexture(textures::pinkShortPaddleTexture);
			UnloadTexture(textures::orangeShortPaddleTexture);
			UnloadTexture(textures::yellowShortPaddleTexture);
			UnloadTexture(textures::grayShortPaddleTexture);
			UnloadTexture(textures::blackShortPaddleTexture);

			// Normal paddles
			UnloadTexture(textures::bluePaddleTexture);
			UnloadTexture(textures::violetPaddleTexture);
			UnloadTexture(textures::greenPaddleTexture);
			UnloadTexture(textures::redPaddleTexture);
			UnloadTexture(textures::pinkPaddleTexture);
			UnloadTexture(textures::orangePaddleTexture);
			UnloadTexture(textures::yellowPaddleTexture);
			UnloadTexture(textures::grayPaddleTexture);
			UnloadTexture(textures::blackPaddleTexture);

			// Long paddles
			UnloadTexture(textures::blueLongPaddleTexture);
			UnloadTexture(textures::violetLongPaddleTexture);
			UnloadTexture(textures::greenLongPaddleTexture);
			UnloadTexture(textures::redLongPaddleTexture);
			UnloadTexture(textures::pinkLongPaddleTexture);
			UnloadTexture(textures::orangeLongPaddleTexture);
			UnloadTexture(textures::yellowLongPaddleTexture);
			UnloadTexture(textures::grayLongPaddleTexture);
			UnloadTexture(textures::blackLongPaddleTexture);			
		}
	}
}