#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

namespace textures
{
	extern Texture2D backgroundGameplay;
	extern Texture2D backgroundMenu;
	extern Texture2D arkanoidTitle;
	extern Texture2D backgroundBorder;
	extern Texture2D ballTexture;
	extern Texture2D blocksTexture;
	extern Texture2D unbreakableBlocks;
	extern Texture2D brokenBlocks;
	extern Texture2D brokenBlocks2;
	extern Texture2D lives;
	extern Texture2D arrows;
	extern Texture2D highScoreMenu;

	// Paddles
	extern Texture2D bluePaddleTexture;
	extern Texture2D violetPaddleTexture;
	extern Texture2D greenPaddleTexture;
	extern Texture2D redPaddleTexture;
	extern Texture2D pinkPaddleTexture;
	extern Texture2D orangePaddleTexture;
	extern Texture2D yellowPaddleTexture;
	extern Texture2D grayPaddleTexture;
	extern Texture2D blackPaddleTexture;

	// Long paddles
	extern Texture2D blueLongPaddleTexture;
	extern Texture2D violetLongPaddleTexture;
	extern Texture2D greenLongPaddleTexture;
	extern Texture2D redLongPaddleTexture;
	extern Texture2D pinkLongPaddleTexture;
	extern Texture2D orangeLongPaddleTexture;
	extern Texture2D yellowLongPaddleTexture;
	extern Texture2D grayLongPaddleTexture;
	extern Texture2D blackLongPaddleTexture;

	// Short paddles
	extern Texture2D blueShortPaddleTexture;
	extern Texture2D violetShortPaddleTexture;
	extern Texture2D greenShortPaddleTexture;
	extern Texture2D redShortPaddleTexture;
	extern Texture2D pinkShortPaddleTexture;
	extern Texture2D orangeShortPaddleTexture;
	extern Texture2D yellowShortPaddleTexture;
	extern Texture2D grayShortPaddleTexture;
	extern Texture2D blackShortPaddleTexture;

	// Buttons
	extern Texture2D playAgainButton;
	extern Texture2D backToMenuButton;
	extern Texture2D startButton;
	extern Texture2D instructionsButton;
	extern Texture2D settingsButton;
	extern Texture2D creditsButton;
	extern Texture2D exitButton;
	extern Texture2D menuCircleButton;
	extern Texture2D settingsCircleButton;
	extern Texture2D exitCircleButton;
	extern Texture2D rightArrow;
	extern Texture2D leftArrow;
	extern Texture2D paddleControls;
	extern Texture2D paddleColor;
	extern Texture2D ballColor;
	extern Texture2D defaultControls;
	extern Texture2D nextLevel;
	extern Texture2D startOver;
	extern Texture2D configurations;
	extern Texture2D resolution;
	extern Texture2D quit;
	extern Texture2D gameAudio;

	// Powerups
	extern Texture2D multiballPowerup;
	extern Texture2D fireballPowerup;
	extern Texture2D paddlePowerup;
	extern Texture2D ballPowerup;
}

#endif