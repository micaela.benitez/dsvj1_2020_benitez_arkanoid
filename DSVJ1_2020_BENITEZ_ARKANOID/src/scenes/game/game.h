#ifndef GAME_H
#define GAME_H

#include "scenes/gameplay/gameplay.h"
#include "scenes/result/result.h"
#include "scenes/menu/menu.h"
#include "scenes/instructions/instructions.h"
#include "scenes/instructions/instructions2.h"
#include "scenes/instructions/instructions3.h"
#include "scenes/instructions/instructions4.h"
#include "scenes/settings/settings.h"
#include "scenes/settings/paddle_controls/paddle_controls.h"
#include "scenes/settings/paddle_color/paddle_color.h"
#include "scenes/settings/ball_color/ball_color.h"
#include "scenes/settings/configurations/configurations.h"
#include "scenes/credits/credits.h"
#include "scenes/credits/credits2.h"
#include "scenes/resolutions/resolutions.h"
#include "scenes/game_audio/game_audio.h"
#include "functions_library/entities/ball.h"
#include "functions_library/entities/paddle.h"
#include "functions_library/entities/block.h"

namespace pong
{
	namespace game
	{
		enum class Scene
		{
			MENU,
			GAMEPLAY,
			RESULT,
			INSTRUCTIONS,
			INSTRUCTIONS2,
			INSTRUCTIONS3,
			INSTRUCTIONS4,
			SETTINGS,
			PADDLECONTROLS,
			PADDLECOLOR,
			BALLCOLOR,
			CONFIGURATIONS,
			CREDITS,
			CREDITS2,
			RESOLUTIONS,
			AUDIO
		};

		extern Scene currentScene;
		extern Scene previousScene;

		extern bool exitButton;

		static void init();
		static void update();
		static void draw();
		static void deinit();

		void run();
	}
}

#endif