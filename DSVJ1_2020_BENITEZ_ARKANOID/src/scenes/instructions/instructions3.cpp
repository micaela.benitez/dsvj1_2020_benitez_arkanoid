#include "instructions3.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace instructions3
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
			{
				leftArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::INSTRUCTIONS2;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::centerDownButtonPosX, pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX,  pos::downButtonsPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::INSTRUCTIONS4;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS3;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS3;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				leftArrowButtonStatus = WHITE;
				menuButtonStatus = WHITE;
				rightArrowButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);

			DrawText("Powerups", pos::instructions3PosX, pos::instructionsPosY, pos::textsSize5, BLACK);
			DrawText("Powerups", pos::instructions3PosX2, pos::instructionsPosY2, pos::textsSize5, WHITE);

			DrawTexture(textures::multiballPowerup, (int)pos::instructions3PowerupPosX, (int)pos::instructions3PowerupPosY, WHITE);
			DrawTexture(textures::fireballPowerup, (int)pos::instructions3PowerupPosX, (int)pos::instructions3PowerupPosY2, WHITE);

			if (screenWidth <= 1000)
			{
				DrawText("MULTIBALL POWERUP", pos::instructions3PosX3, pos::instructions3PosY, pos::textsSize4, WHITE);
				DrawText("2 extra balls appear in the game from the last ball position, once", pos::instructions3PosX4, pos::instructions3PosY2, pos::textsSize4, WHITE);
				DrawText("they are lost they cannot be recovered until the same powerup is", pos::instructions3PosX5, pos::instructions3PosY3, pos::textsSize4, WHITE);
				DrawText("is caught again. Advantage: if you lose them, they do not take life!", pos::instructions3PosX6, pos::instructions3PosY4, pos::textsSize4, WHITE);
								
				DrawText("FIREBALL POWERUP", pos::instructions3PosX7, pos::instructions3PosY5, pos::textsSize4, WHITE);
				DrawText("The ball will go through all the blocks breaking all of them in 1", pos::instructions3PosX8, pos::instructions3PosY6, pos::textsSize4, WHITE);
				DrawText("hit! (Except for unbreakable blocks)", pos::instructions3PosX9, pos::instructions3PosY7, pos::textsSize4, WHITE);
			}
			else
			{				
				DrawText("MULTIBALL POWERUP", pos::instructions3PosX10, pos::instructions3PosY, pos::textsSize3, WHITE);
				DrawText("2 extra balls appear in the game from the last ball position, once", pos::instructions3PosX11, pos::instructions3PosY2, pos::textsSize3, WHITE);
				DrawText("they are lost they cannot be recovered until the same powerup is", pos::instructions3PosX12, pos::instructions3PosY3, pos::textsSize3, WHITE);
				DrawText("is caught again. Advantage: if you lose them, they do not take life!", pos::instructions3PosX13, pos::instructions3PosY4, pos::textsSize3, WHITE);

				DrawText("FIREBALL POWERUP", pos::instructions3PosX14, pos::instructions3PosY5, pos::textsSize3, WHITE);
				DrawText("The ball will go through all the blocks breaking all of them in 1", pos::instructions3PosX15, pos::instructions3PosY6, pos::textsSize3, WHITE);
				DrawText("hit! (Except for unbreakable blocks)", pos::instructions3PosX16, pos::instructions3PosY7, pos::textsSize3, WHITE);
			}

			DrawTexture(textures::leftArrow, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, leftArrowButtonStatus);
			DrawTexture(textures::menuCircleButton, (int)pos::centerDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::rightArrow, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, rightArrowButtonStatus);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		} 

		void deinit()
		{
		}
	}
}
