#ifndef SOUNDS_H
#define SOUNDS_H

struct Sounds
{
	bool ballCollisionEntities;
	bool ballCollisionWalls;
	bool death;
	bool powerup;
	bool button;
	bool winner;
	bool loser;
};

#endif 