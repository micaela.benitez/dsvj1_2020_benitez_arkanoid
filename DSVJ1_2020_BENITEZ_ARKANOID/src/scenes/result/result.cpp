#include "result.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace result
	{
		static Color playAgainButtonStatus = WHITE;
		static Color backToMenuButtonStatus = WHITE;
		static Color exitButtonStatus = WHITE;
		static Color nextLevelStatus = WHITE;
		static Color startOverStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;
		
		void init()
		{
			textures::playAgainButton = LoadTexture("res/raw/textures/playagain.png");
			textures::playAgainButton.width = sizes::buttonsWidth;
			textures::playAgainButton.height = sizes::buttonsHeight;

			textures::backToMenuButton = LoadTexture("res/raw/textures/backtomenu.png");
			textures::backToMenuButton.width = sizes::buttonsWidth;
			textures::backToMenuButton.height = sizes::buttonsHeight;

			textures::nextLevel = LoadTexture("res/raw/textures/nextlevel.png");
			textures::nextLevel.width = sizes::buttonsWidth;
			textures::nextLevel.height = sizes::buttonsHeight;

			textures::startOver = LoadTexture("res/raw/textures/startover.png");
			textures::startOver.width = sizes::buttonsWidth;
			textures::startOver.height = sizes::buttonsHeight;
		}

		void update()
		{
			audio::resultAudio();
			sounds.winner = false;
			sounds.loser = false;
			
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::resultLeftButtons, pos::resultUpButtons, (float)textures::playAgainButton.width, (float)textures::playAgainButton.height }))
			{
				playAgainButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::GAMEPLAY;
					actualScore = scorePerLevel;
					gameplay::initialize();
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resultRightButtons, pos::resultUpButtons, (float)textures::backToMenuButton.width, (float)textures::backToMenuButton.height }))
			{
				backToMenuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resultLeftButtons, pos::resultDownButtons, (float)textures::nextLevel.width, (float)textures::nextLevel.height }) && pong::gameplay::playerLives > 0)
			{
				nextLevelStatus = GRAY;
				startOverStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;

					if (level == 1 || level == 2)
					{
						level++;
						pong::game::currentScene = pong::game::Scene::GAMEPLAY;
						gameplay::initialize();
					}
					else
					{
						level = 1;
						actualScore = 0;
						pong::game::currentScene = pong::game::Scene::GAMEPLAY;
						gameplay::initialize();
					}
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resultRightButtons, pos::resultDownButtons, (float)textures::exitButton.width, (float)textures::exitButton.height }) && pong::gameplay::playerLives > 0
				  || CheckCollisionPointRec(mousePoint, { pos::resultCenterButton, pos::resultDownButtons, (float)textures::exitButton.width, (float)textures::exitButton.height }) && pong::gameplay::playerLives <= 0)
			{
				exitButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::exitButton = true;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::RESULT;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::RESULT;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				playAgainButtonStatus = WHITE;
				backToMenuButtonStatus = WHITE;
				exitButtonStatus = WHITE;
				nextLevelStatus = WHITE;
				startOverStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::resultAudio();
			sounds.button = false;
		}

		void draw()
		{
			drawScreenDetails();

			DrawTexture(textures::backgroundGameplay, (int)pos::backgroundPos, informationBorderSize, WHITE);
			DrawTexture(textures::backgroundBorder, (int)pos::backgroundPos, informationBorderSize, WHITE);

			DrawText("Game Over", pos::gameOverPosX, pos::gameOverPosY, pos::textsSize2, WHITE);
			DrawTexture(textures::playAgainButton, (int)pos::resultLeftButtons, (int)pos::resultUpButtons, playAgainButtonStatus);
			DrawTexture(textures::backToMenuButton, (int)pos::resultRightButtons, (int)pos::resultUpButtons, backToMenuButtonStatus);

			if (pong::gameplay::playerLives <= 0)
			{
				DrawText("YOU LOSE", pos::resultPosX, pos::resultPosY, pos::textsSize6, RED);
				DrawTexture(textures::exitButton, (int)pos::resultCenterButton, (int)pos::resultDownButtons, exitButtonStatus);
			}
			else
			{
				DrawText(" YOU WIN!", pos::resultPosX, pos::resultPosY, pos::textsSize6, GREEN);
				DrawTexture(textures::exitButton, (int)pos::resultRightButtons, (int)pos::resultDownButtons, exitButtonStatus);
				if (level == 1 || level == 2) DrawTexture(textures::nextLevel, (int)pos::resultLeftButtons, (int)pos::resultDownButtons, nextLevelStatus);
				else DrawTexture(textures::startOver, (int)pos::resultLeftButtons, (int)pos::resultDownButtons, startOverStatus);
			}

			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::playAgainButton);
			UnloadTexture(textures::backToMenuButton);
			UnloadTexture(textures::nextLevel);
			UnloadTexture(textures::startOver);
		}
	}
}