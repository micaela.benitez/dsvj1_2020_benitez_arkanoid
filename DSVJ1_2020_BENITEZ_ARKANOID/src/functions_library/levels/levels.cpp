#include "levels.h"

using namespace pong;
using namespace gameplay;

void level1()
{
    for (int i = 0; i < blocksPerLine; i++)
    {
        for (int j = 0; j < blocksPerColumn; j++)
        {
            if (block[i][j].active)
            {
                if (i >= 0 && i <= 1) block[i][j].strong = true;
                else block[i][j].hits = block[i][j].weakHits;

                if (block[i][j].hits == 0 && block[i][j].strong) block[i][j].currentBlockTexture = textures::blocksTexture;
                else if (block[i][j].hits == 1 && block[i][j].strong) block[i][j].currentBlockTexture = textures::brokenBlocks;
                else if (block[i][j].hits == 2 && block[i][j].strong) block[i][j].currentBlockTexture = textures::brokenBlocks2;
                else if (!block[i][j].strong) block[i][j].currentBlockTexture = textures::blocksTexture;

                if (i == 3 && (j == 1 || j == 3 || j == 5 || j == 7))
                {
                    DrawTexture(textures::unbreakableBlocks, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), WHITE);
                    block[i][j].unbreakable = true;
                }
                else if (((i == 0 || i == 6) && j >= 3 && j <= 5) || ((i == 1 || i == 5) && (j == 0 || j == 8)) || (i == 3 && (j == 0 || j == 4 || j == 8))) block[i][j].active = false;
                else if (i == 0) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), VIOLET);
                else if (i == 1) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), BLUE);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 3) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), RED);
                else if (i == 4) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), PINK);
                else if (i == 5) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), ORANGE);
                else if (i == 6) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), YELLOW);
                #if DEBUG 
                DrawRectangleLines(block[i][j].position.x - (block[i][j].size.x / 2), block[i][j].position.y - (block[i][j].size.y / 2), textures::blocksTexture.width, textures::blocksTexture.height, WHITE);
                #endif
            }
        }
    }
}

void level2()
{
    for (int i = 0; i < blocksPerLine; i++)
    {
        for (int j = 0; j < blocksPerColumn; j++)
        {
            if (block[i][j].active)
            {
                if (i >= 0 && i <= 3) block[i][j].strong = true;
                else block[i][j].hits = block[i][j].weakHits;

                if (block[i][j].hits == 0 && block[i][j].strong) block[i][j].currentBlockTexture = textures::blocksTexture;
                else if (block[i][j].hits == 1 && block[i][j].strong) block[i][j].currentBlockTexture = textures::brokenBlocks;
                else if (block[i][j].hits == 2 && block[i][j].strong) block[i][j].currentBlockTexture = textures::brokenBlocks2;
                else if (!block[i][j].strong) block[i][j].currentBlockTexture = textures::blocksTexture;

                if (((i == 1 || i == 5) && (j == 3 || j == 5)) || (i == 3 && (j == 1 || j == 7)))
                {
                    DrawTexture(textures::unbreakableBlocks, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), WHITE);
                    block[i][j].unbreakable = true;
                }
                else if (((i == 0 || i == 2 || i == 4 || i == 6) && (j == 1 || j == 3 || j == 5 || j == 7)) || ((i == 1 || i == 3 || i == 5 || i == 7) && (j == 0 || j == 2 || j == 4 || j == 6 || j == 8))) block[i][j].active = false;
                else if (i == 0) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), VIOLET);
                else if (i == 1) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), BLUE);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 3) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), RED);
                else if (i == 4) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), PINK);
                else if (i == 5) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), ORANGE);
                else if (i == 6) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), YELLOW);
#if DEBUG 
                DrawRectangleLines(block[i][j].position.x - (block[i][j].size.x / 2), block[i][j].position.y - (block[i][j].size.y / 2), textures::blocksTexture.width, textures::blocksTexture.height, WHITE);
#endif
            }
        }
    }
}

void level3()
{
    for (int i = 0; i < blocksPerLine; i++)
    {
        for (int j = 0; j < blocksPerColumn; j++)
        {
            if (block[i][j].active)
            {
                block[i][j].strong = true;

                if (block[i][j].hits == 0 && block[i][j].strong) block[i][j].currentBlockTexture = textures::blocksTexture;
                else if (block[i][j].hits == 1 && block[i][j].strong) block[i][j].currentBlockTexture = textures::brokenBlocks;
                else if (block[i][j].hits == 2 && block[i][j].strong) block[i][j].currentBlockTexture = textures::brokenBlocks2;

                if (i == 6 && j != 0 && j != 8 && j != 4)
                {
                    DrawTexture(textures::unbreakableBlocks, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), WHITE);
                    block[i][j].unbreakable = true;
                }
                else if (((j == 0 || j == 8) && i != 6) || ((j == 1 || j == 7) && (i != 5 && i != 4)) || ((j == 2 || j == 6) && (i == 0 || i == 1))) block[i][j].active = false;
                else if (i == 0) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), VIOLET);
                else if (i == 1) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), BLUE);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 2) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), LIME);
                else if (i == 3) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), RED);
                else if (i == 4) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), PINK);
                else if (i == 5) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), ORANGE);
                else if (i == 6) DrawTexture(block[i][j].currentBlockTexture, (int)(block[i][j].position.x - (block[i][j].size.x / 2)), (int)(block[i][j].position.y - (block[i][j].size.y / 2)), YELLOW);
                #if DEBUG 
                DrawRectangleLines(block[i][j].position.x - (block[i][j].size.x / 2), block[i][j].position.y - (block[i][j].size.y / 2), textures::blocksTexture.width, textures::blocksTexture.height, WHITE);
                #endif
            }
        }
    }
}

void levels()
{
    if (level == 1)  level1();
    else if (level == 2) level2();
    else level3();
}