#include "instructions2.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace instructions2
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
			{
				leftArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::INSTRUCTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::centerDownButtonPosX,  pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX,  pos::downButtonsPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::INSTRUCTIONS3;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS2;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS2;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				leftArrowButtonStatus = WHITE;
				menuButtonStatus = WHITE;
				rightArrowButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);

			DrawText("Blocks and paddle", pos::instructions2PosX, pos::instructionsPosY, pos::textsSize5, BLACK);
			DrawText("Blocks and paddle", pos::instructions2PosX2, pos::instructionsPosY2, pos::textsSize5, WHITE);

			if (screenWidth <= 1000)
			{
				DrawText("  Blocks that                 Blocks that                 Unbreakable", pos::instructions2PosX3, pos::instructionsPosY3, pos::textsSize4, WHITE);
				DrawText("break with 1 hit            break with 3 hits                 blocks", pos::instructions2PosX3, pos::instructionsPosY4, pos::textsSize4, WHITE);
				DrawText("(This key can                                                      (This key can", pos::instructions2PosX4, pos::instructions2PosY, pos::textsSize4, WHITE);
				DrawText(" be changed)                                                         be changed)", pos::instructions2PosX4, pos::instructions2PosY2, pos::textsSize4, WHITE);
				DrawTexture(textures::blocksTexture, (int)pos::instructions2Block1PosX, (int)pos::instructions2Block1PosY, ORANGE);
				DrawTexture(textures::blocksTexture, (int)pos::instructions2Block2PosX, (int)pos::instructions2Block1PosY, ORANGE);
				DrawTexture(textures::brokenBlocks, (int)pos::instructions2Block2PosX, (int)pos::instructions2Block2PosY, ORANGE);
				DrawTexture(textures::brokenBlocks2, (int)pos::instructions2Block2PosX, (int)pos::instructions2Block3PosY, ORANGE);
				DrawTexture(textures::unbreakableBlocks, (int)pos::instructions2Block3PosX, (int)pos::instructions2Block1PosY, WHITE);
				DrawRectangleLinesEx({ pos::instructions2Rec1PosX, pos::instructions2RecPosY, (float)pos::textsSize7, (float)pos::textsSize7 }, (int)pos::recSizeLines, WHITE);
				DrawRectangleLinesEx({ pos::instructions2Rec2PosX, pos::instructions2RecPosY, (float)pos::textsSize7, (float)pos::textsSize7 }, (int)pos::recSizeLines, WHITE);
				DrawText("<-", pos::instructions2PosX5, pos::instructions2PosY3, pos::textsSize3, WHITE);
				DrawText("->", pos::instructions2PosX6, pos::instructions2PosY3, pos::textsSize3, WHITE);
			}
			else
			{				
				DrawText("  Blocks that                 Blocks that                 Unbreakable", pos::instructions2PosX7, pos::instructionsPosY3, pos::textsSize3, WHITE);
				DrawText("break with 1 hit            break with 3 hits                 blocks", pos::instructions2PosX7, pos::instructionsPosY4, pos::textsSize3, WHITE);
				DrawText("(This key can                                           (This key can", pos::instructions2PosX8, pos::instructions2PosY, pos::textsSize3, WHITE);
				DrawText(" be changed)                                              be changed)", pos::instructions2PosX8, pos::instructions2PosY2, pos::textsSize3, WHITE);
				DrawTexture(textures::blocksTexture, (int)pos::instructions2Block1PosX2, (int)pos::instructions2Block1PosY, ORANGE);
				DrawTexture(textures::blocksTexture, (int)pos::instructions2Block2PosX, (int)pos::instructions2Block1PosY, ORANGE);
				DrawTexture(textures::brokenBlocks, (int)pos::instructions2Block2PosX, (int)pos::instructions2Block2PosY, ORANGE);
				DrawTexture(textures::brokenBlocks2, (int)pos::instructions2Block2PosX, (int)pos::instructions2Block3PosY, ORANGE);
				DrawTexture(textures::unbreakableBlocks, (int)pos::instructions2Block3PosX2, (int)pos::instructions2Block1PosY, WHITE);
				DrawRectangleLinesEx({ pos::instructions2Rec1PosX2, pos::instructions2RecPosY, (float)pos::textsSize6, (float)pos::textsSize6 }, (int)pos::recSizeLines, WHITE);
				DrawRectangleLinesEx({ pos::instructions2Rec2PosX2, pos::instructions2RecPosY, (float)pos::textsSize6, (float)pos::textsSize6 }, (int)pos::recSizeLines, WHITE);
				DrawText("<-", pos::instructions2PosX9, pos::instructions2PosY3, pos::keysSize, WHITE);
				DrawText("->", pos::instructions2PosX10, pos::instructions2PosY3, pos::keysSize, WHITE);
			}						

			if (screenHeight <= 800) DrawTexture(textures::bluePaddleTexture, (int)pos::instructions2RecPosX, (int)pos::instructions2Rec1PosY, WHITE);
			else DrawTexture(textures::bluePaddleTexture, (int)pos::instructions2RecPosX, (int)pos::instructions2Rec2PosY, WHITE);

			DrawTexture(textures::leftArrow, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, leftArrowButtonStatus);
			DrawTexture(textures::menuCircleButton, (int)pos::centerDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::rightArrow, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, rightArrowButtonStatus);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
		}
	}
}
