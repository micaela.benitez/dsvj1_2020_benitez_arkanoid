#include "powerups.h"

using namespace pong;
using namespace gameplay;

void multiballPowerup()
{
    using namespace balls;
    
    if (powerup.multiball1)
    {
        ball1.loss = false;
        if (ball1.position.y >= (screenHeight - ball1.radius)) powerup.multiball1 = false;
    }
    else
    {
        ball1.position = { ball.position.x, ball.position.y };
        ball1.speed = { ball.speed.x, speedYBallModifier * GetFrameTime() };
    }

    if (powerup.multiball2)
    {
        ball2.loss = false;
        if (ball2.position.y >= (screenHeight - ball2.radius)) powerup.multiball2 = false;
    }
    else
    {
        ball2.position = { ball.position.x, ball.position.y };
        ball2.speed = { -ball.speed.x, speedYBallModifier * GetFrameTime() };
    }
}

void fireballPowerup()
{
    static const int timeUp = 100;  

    if (powerup.fireball)
    {
        timer--;
        if (timer == timeUp) powerup.fireball = false;        
    }
}

void paddlePowerup()
{
    if (powerup.paddle && powerup.longPaddle) player.width = (float)playerWidthLong;
    else if (powerup.paddle && !powerup.longPaddle) player.width = (float)playerWidthShort;
    else player.width = paddle.size.x;
}

void ballPowerup(Ball& ball)
{
    static const float slowSpeedX = 300.0f;    
    static const float slowSpeedY = 300.0f;
    static float fastSpeedX = 0.0f;
    fastSpeedX = speedXBallModifier;

    if (powerup.ball)
    {
        speedXBallModifier = slowSpeedX;
        if (ball.speed.y >= 0) ball.speed.y = slowSpeedY * GetFrameTime();
        else ball.speed.y = -slowSpeedY * GetFrameTime();
    }
    else speedXBallModifier = fastSpeedX;
}