#include "instructions4.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace instructions4
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
			{
				leftArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::INSTRUCTIONS3;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX,  pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS4;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS4;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				leftArrowButtonStatus = WHITE;
				menuButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);

			DrawText("Powerups", pos::instructions3PosX, pos::instructionsPosY, pos::textsSize5, BLACK);
			DrawText("Powerups", pos::instructions3PosX2, pos::instructionsPosY2, pos::textsSize5, WHITE);

			DrawTexture(textures::paddlePowerup, (int)pos::instructions3PowerupPosX, (int)pos::instructions3PowerupPosY, WHITE);
			DrawTexture(textures::ballPowerup, (int)pos::instructions3PowerupPosX, (int)pos::instructions4PowerupPosY3, WHITE);

			if (screenWidth <= 1000)
			{
				DrawText("PADDLE POWERUP", pos::instructions4PosX, pos::instructions4PosY, pos::textsSize4, WHITE);
				DrawText("The width of the paddle can increase or decrease until you lose", pos::instructions4PosX2, pos::instructions4PosY2, pos::textsSize4, WHITE);
				DrawText("the next life or win the level.", pos::instructions4PosX3, pos::instructions4PosY3, pos::textsSize4, WHITE);

				DrawText("BALL POWERUP", pos::instructions4PosX4, pos::instructions4PosY4, pos::textsSize4, WHITE);
				DrawText("The speed of the ball decreases until you lose the next life the", pos::instructions4PosX2, pos::instructions4PosY5, pos::textsSize4, WHITE);
				DrawText("or win the level.", pos::instructions4PosX4, pos::instructions4PosY6, pos::textsSize4, WHITE);
			}
			else
			{
				DrawText("PADDLE POWERUP", pos::instructions4PosX3, pos::instructions4PosY, pos::textsSize3, WHITE);
				DrawText("The width of the paddle can increase or decrease until you lose", pos::instructions4PosX5, pos::instructions4PosY2, pos::textsSize3, WHITE);
				DrawText("the next life or win the level.", pos::instructions4PosX6, pos::instructions4PosY3, pos::textsSize3, WHITE);

				DrawText("BALL POWERUP", pos::instructions4PosX7, pos::instructions4PosY4, pos::textsSize3, WHITE);
				DrawText("The speed of the ball decreases until you lose the next life the", pos::instructions4PosX5, pos::instructions4PosY5, pos::textsSize3, WHITE);
				DrawText("or win the level.", pos::instructions4PosX7, pos::instructions4PosY6, pos::textsSize3, WHITE);
			}

			DrawText("GOOD LUCK!", pos::instructions4PosX8, pos::instructions4PosY7, pos::textsSize, BLACK);
			DrawText("GOOD LUCK!", pos::instructions4PosX7, pos::instructions4PosY8, pos::textsSize, WHITE);

			DrawTexture(textures::leftArrow, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, leftArrowButtonStatus);
			DrawTexture(textures::menuCircleButton, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
		}
	}
}
