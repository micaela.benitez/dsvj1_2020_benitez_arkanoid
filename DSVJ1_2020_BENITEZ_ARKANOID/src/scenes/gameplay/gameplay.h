#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

#include "scenes/game/game.h"
#include "functions_library/drawings/drawings.h"
#include "functions_library/entities_motion/entities_motion.h"
#include "functions_library/textures/textures.h"
#include "functions_library/condition_to_win/condition_to_win.h"
#include "functions_library/powerups/powerups.h"
#include "functions_library/audio/audio.h"
#include "functions_library/audio/sounds.h"
#include "functions_library/sizes/sizes.h"
#include "functions_library/positions/positions.h"
#include "functions_library/levels/levels.h"

const int blocksPerLine = 7;
const int blocksPerColumn = 9;

namespace pong
{
	namespace balls
	{
		extern Ball ball;
		extern Ball ball1;
		extern Ball ball2;
	}

	namespace gameplay
	{		
		extern int screenWidth;
		extern int screenHeight;
		
		extern int playerTotalLives;
		extern int playerLives;

		extern Paddle paddle;
		
		extern Block block[blocksPerLine][blocksPerColumn];

		extern Rectangle player;
		extern int playerWidthLong;
		extern int playerWidthShort;

		extern int borderSize;
		extern int informationBorderSize;

		extern int actualScore;
		extern int highScore;
		extern int scorePerLevel;
		extern bool playerWon;

		extern Vector2 mousePoint;

		extern float speedXBallModifier;
		extern float speedYBallModifier;
		extern float speedPaddleModifier;

		extern int timer;

		struct Powerup
		{
			bool multiball1;
			bool multiball2;
			bool fireball;
			bool paddle;
			bool longPaddle;
			bool ball;
		};
		extern Powerup powerup;

		extern Sounds sounds;

		extern int level;

		extern Color audioButtonStatus;
		
		void init();
		void initialize();
		void update();
		void draw();
		void deinit();
	}
}

#endif