#include "game.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace game
	{
		Scene currentScene;
		Scene previousScene;

		bool exitButton;

		static void init()
		{
			InitWindow(screenWidth, screenHeight, "ARKANOID");
			InitAudioDevice();
			SetTargetFPS(60);

			currentScene = Scene::MENU;

			menu::init();
			gameplay::init();
			result::init();
			instructions::init();
			instructions2::init();
			instructions3::init();
			instructions4::init();
			settings::init();
			paddle_controls::init();
			paddle_color::init();
			ball_color::init();
			configurations::init();
			credits::init();
			credits2::init();
			resolutions::init();
			game_audio::init();
		}

		static void update()
		{
			SetWindowSize(screenWidth, screenHeight);			
			
			switch (currentScene)
			{
			case Scene::MENU:
				menu::update();
				break;
			case Scene::GAMEPLAY:
				gameplay::update();
				break;
			case Scene::RESULT:
				result::update();
				break;
			case Scene::INSTRUCTIONS:
				instructions::update();
				break;
			case Scene::INSTRUCTIONS2:
				instructions2::update();
				break;
			case Scene::INSTRUCTIONS3:
				instructions3::update();
				break;
			case Scene::INSTRUCTIONS4:
				instructions4::update();
				break;
			case Scene::SETTINGS:
				settings::update();
				break;
			case Scene::PADDLECONTROLS:
				paddle_controls::update();
				break;
			case Scene::PADDLECOLOR:
				paddle_color::update();
				break;
			case Scene::BALLCOLOR:
				ball_color::update();
				break;
			case Scene::CONFIGURATIONS:
				configurations::update();
				break;
			case Scene::CREDITS:
				credits::update();
				break;
			case Scene::CREDITS2:
				credits2::update();
				break;
			case Scene::RESOLUTIONS:
				resolutions::update();
				break;
			case Scene::AUDIO:
				game_audio::update();
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			
			switch (currentScene)
			{
			case Scene::MENU:
				menu::draw();
				break;
			case Scene::GAMEPLAY:
				gameplay::draw();
				break;
			case Scene::RESULT:
				result::draw();
				break;
			case Scene::INSTRUCTIONS:
				instructions::draw();
				break;
			case Scene::INSTRUCTIONS2:
				instructions2::draw();
				break;
			case Scene::INSTRUCTIONS3:
				instructions3::draw();
				break;
			case Scene::INSTRUCTIONS4:
				instructions4::draw();
				break;
			case Scene::SETTINGS:
				settings::draw();
				break;
			case Scene::PADDLECONTROLS:
				paddle_controls::draw();
				break;
			case Scene::PADDLECOLOR:
				paddle_color::draw();
				break;
			case Scene::BALLCOLOR:
				ball_color::draw();
				break;
			case Scene::CONFIGURATIONS:
				configurations::draw();
				break;
			case Scene::CREDITS:
				credits::draw();
				break;
			case Scene::CREDITS2:
				credits2::draw();
				break;
			case Scene::RESOLUTIONS:
				resolutions::draw();
				break;
			case Scene::AUDIO:
				game_audio::draw();
				break;
			}

			EndDrawing();
		}

		static void deinit()
		{
			CloseWindow();

			menu::deinit();
			gameplay::deinit();
			result::deinit();
			instructions::deinit();
			instructions2::deinit();
			instructions3::deinit();
			instructions4::deinit();
			settings::deinit();
			paddle_controls::deinit();
			paddle_color::deinit();
			ball_color::deinit();
			configurations::deinit();
			credits::deinit();
			credits2::deinit();
			resolutions::deinit();
			game_audio::deinit();
		}

		void run()
		{
			init();

			while (!WindowShouldClose() && !exitButton)
			{
				update();
				draw();
			}

			deinit();
		}
	}
}