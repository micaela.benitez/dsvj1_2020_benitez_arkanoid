#include "credits.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace credits
	{
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX,  pos::downButtonsPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::CREDITS2;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::CREDITS;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::CREDITS;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				menuButtonStatus = WHITE;
				rightArrowButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawText("Credits", pos::creditsPosX, pos::creditsPosY, pos::textsSize5, BLACK);
			DrawText("Credits", pos::creditsPosX2, pos::creditsPosY2, pos::textsSize5, WHITE);
			DrawRectangle((int)pos::creditsRec1PosX, (int)pos::creditsRecPosY, (int)pos::creditsRecSize, (int)pos::recSizeLines, BLACK);
			DrawRectangle((int)pos::creditsRec2PosX, (int)pos::creditsRecPosY, (int)pos::creditsRecSize, (int)pos::recSizeLines, BLACK);
			DrawText("Arkanoid V 2.0", pos::versionPosX2, pos::versionPosY, pos::textsSize3, WHITE);

			DrawText("Created by", pos::creditsPosX3, pos::creditsPosY3, pos::textsSize, WHITE);
			DrawText("Micaela Luz Benitez", pos::creditsPosX4, pos::creditsPosY4, pos::textsSize3, BLACK);

			DrawText("Textures", pos::creditsPosX5, pos::creditsPosY5, pos::textsSize, WHITE);
			DrawText("Micaela Luz Benitez", pos::creditsPosX6, pos::creditsPosY6, pos::textsSize3, BLACK);
			DrawText("Raf_rgb from youtube.com", pos::creditsPosX7, pos::creditsPosY7, pos::textsSize3, BLACK);
			DrawText("Kenney Vleugels from kenney.nl", pos::creditsPosX8, pos::creditsPosY8, pos::textsSize3, BLACK);
			DrawText("Freepik.es", pos::creditsPosX9, pos::creditsPosY9, pos::textsSize3, BLACK);
			DrawText("Vgmpf.com", pos::creditsPosX10, pos::creditsPosY10, pos::textsSize3, BLACK);
			DrawText("Pngjoy.com", pos::creditsPosX11, pos::creditsPosY11, pos::textsSize3, BLACK);

			DrawTexture(textures::menuCircleButton, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::rightArrow, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, rightArrowButtonStatus);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{

		}
	}
}