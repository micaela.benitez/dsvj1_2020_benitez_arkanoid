#include "configurations.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace configurations
	{
		bool powerupsConfigurationActive = true;
		
		static Color menuButtonStatus = WHITE;
		static Color settingsButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		static Color easyStatus = WHITE;
		static Color normalStatus = BLACK;
		static Color hardStatus = BLACK;
		static Color threeStatus = BLACK;
		static Color fiveStatus = WHITE;
		static Color sevenStatus = BLACK;
		static Color yesStatus = WHITE;
		static Color noStatus = BLACK;
		
		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX, pos::downButtonsPosY, (float)textures::settingsCircleButton.width, (float)textures::settingsCircleButton.height }))
			{
				settingsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::SETTINGS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX, pos::configurationsRecPosY, pos::configurationsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					easyStatus = WHITE;
					normalStatus = BLACK;
					hardStatus = BLACK;
					speedXBallModifier = 400.0f;
					speedYBallModifier = -400.0f;
					speedPaddleModifier = 350.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX2, pos::configurationsRecPosY, pos::configurationsRecWidth2, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					easyStatus = BLACK;
					normalStatus = WHITE;
					hardStatus = BLACK;
					speedXBallModifier = 500.0f;
					speedYBallModifier = -500.0f;
					speedPaddleModifier = 400.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX3, pos::configurationsRecPosY, pos::configurationsRecWidth, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					easyStatus = BLACK;
					normalStatus = BLACK;
					hardStatus = WHITE;
					speedXBallModifier = 600.0f;
					speedYBallModifier = -600.0f;
					speedPaddleModifier = 450.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX4, pos::configurationsRecPosY2, pos::configurationsRecWidth3, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					threeStatus = WHITE;
					fiveStatus = BLACK;
					sevenStatus = BLACK;
					playerTotalLives = 3;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX5, pos::configurationsRecPosY2, pos::configurationsRecWidth3, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					threeStatus = BLACK;
					fiveStatus = WHITE;
					sevenStatus = BLACK;
					playerTotalLives = 5;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX6, pos::configurationsRecPosY2, pos::configurationsRecWidth3, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					threeStatus = BLACK;
					fiveStatus = BLACK;
					sevenStatus = WHITE;
					playerTotalLives = 7;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX7, pos::configurationsRecPosY3, pos::configurationsRecWidth4, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					yesStatus = WHITE;
					noStatus = BLACK;
					powerupsConfigurationActive = true;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::configurationsRecPosX8, pos::configurationsRecPosY3, pos::configurationsRecWidth4, pos::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					yesStatus = BLACK;
					noStatus = WHITE;
					powerupsConfigurationActive = false;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::CONFIGURATIONS;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
			audioButtonStatus = GRAY;
			if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				pong::game::previousScene = pong::game::Scene::CONFIGURATIONS;
				pong::game::currentScene = pong::game::Scene::AUDIO;
			}
			}
			else
			{
				menuButtonStatus = WHITE;
				settingsButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawTexture(textures::menuCircleButton, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::settingsCircleButton, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, settingsButtonStatus);

			DrawText("Configurations", pos::configurationsTextPosX, pos::colorTextPosY, pos::textsSize5, BLACK);
			DrawText("Configurations", pos::configurationsTextPosX2, pos::colorTextPosY2, pos::textsSize5, WHITE);

			DrawText("Paddle and ball speed", pos::configurationsPosX, pos::configurationsPosY, pos::textsSize, BLACK);
			DrawText("Easy", pos::configurationsPosX2, pos::configurationsPosY2, pos::textsSize, easyStatus);
			DrawText("Normal", pos::configurationsPosX3, pos::configurationsPosY2, pos::textsSize, normalStatus);
			DrawText("Hard", pos::configurationsPosX4, pos::configurationsPosY2, pos::textsSize, hardStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX, pos::configurationsRecPosY, pos::configurationsRecWidth, pos::recHeight }, pos::recSizeLines, easyStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX2, pos::configurationsRecPosY, pos::configurationsRecWidth2, pos::recHeight }, pos::recSizeLines, normalStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX3, pos::configurationsRecPosY, pos::configurationsRecWidth, pos::recHeight }, pos::recSizeLines, hardStatus);

			DrawText("Lives", pos::configurationsPosX5, pos::configurationsPosY3, pos::textsSize, BLACK);
			DrawText("3", pos::configurationsPosX6, pos::configurationsPosY4, pos::textsSize, threeStatus);
			DrawText("5", pos::configurationsPosX7, pos::configurationsPosY4, pos::textsSize, fiveStatus);
			DrawText("7", pos::configurationsPosX8, pos::configurationsPosY4, pos::textsSize, sevenStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX4, pos::configurationsRecPosY2, pos::configurationsRecWidth3, pos::recHeight }, pos::recSizeLines, threeStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX5, pos::configurationsRecPosY2, pos::configurationsRecWidth3, pos::recHeight }, pos::recSizeLines, fiveStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX6, pos::configurationsRecPosY2, pos::configurationsRecWidth3, pos::recHeight }, pos::recSizeLines, sevenStatus);

			DrawText("Powerups", pos::configurationsPosX9, pos::configurationsPosY5, pos::textsSize, BLACK);
			DrawText("Yes", pos::configurationsPosX10, pos::configurationsPosY6, pos::textsSize, yesStatus);
			DrawText("No", pos::configurationsPosX11, pos::configurationsPosY6, pos::textsSize, noStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX7, pos::configurationsRecPosY3, pos::configurationsRecWidth4, pos::recHeight }, pos::recSizeLines, yesStatus);
			DrawRectangleLinesEx({ pos::configurationsRecPosX8, pos::configurationsRecPosY3, pos::configurationsRecWidth4, pos::recHeight }, pos::recSizeLines, noStatus);

			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{

		}
	}
}
