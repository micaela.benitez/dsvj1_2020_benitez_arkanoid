#include "paddle_controls.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace paddle_controls
	{
		Keys key;

		Color leftArrowButtonStatus = WHITE;
		Color rightArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color settingsButtonStatus = WHITE;
		static Color defaultControlsButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;
		static Color rec1Status = BLACK;
		static Color rec2Status = BLACK;
		static bool rec1Active = false;
		static bool rec2Active = false;

		void init()
		{
			textures::defaultControls = LoadTexture("res/raw/textures/defaultcontrols.png");
			textures::defaultControls.width = sizes::buttonsWidth;
			textures::defaultControls.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::paddleControlsRec1PosX, pos::paddleControlsRecPosY, pos::paddleControlsrecSize, pos::paddleControlsrecSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				rec1Status = WHITE;
				rec2Status = BLACK;
				rec1Active = true;
				rec2Active = false;
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::paddleControlsRec2PosX, pos::paddleControlsRecPosY, pos::paddleControlsrecSize, pos::paddleControlsrecSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				rec2Status = WHITE;
				rec1Status = BLACK;
				rec2Active = true;
				rec1Active = false;
			}
			else if (IsKeyPressed(KEY_ENTER))
			{
				rec1Status = BLACK;
				rec2Status = BLACK;
				rec1Active = false;
				rec2Active = false;
			}

			if (rec1Active) changeKey(key.leftPaddle, key.rightPaddle, key.leftNumber);
			else if (rec2Active) changeKey(key.rightPaddle, key.leftPaddle, key.rightNumber);

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX, pos::downButtonsPosY, (float)textures::settingsCircleButton.width, (float)textures::settingsCircleButton.height }))
			{
				settingsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::SETTINGS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::paddleControlsDefaultPosX, pos::downButtonsPosY, (float)textures::defaultControls.width, (float)textures::defaultControls.height }))
			{
				defaultControlsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					key.leftPaddle = KEY_LEFT;
					key.rightPaddle = KEY_RIGHT;
					rec1Status = BLACK;
					rec2Status = BLACK;
					rec1Active = false;
					rec2Active = false;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::PADDLECONTROLS;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::PADDLECONTROLS;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				menuButtonStatus = WHITE;
				settingsButtonStatus = WHITE;
				defaultControlsButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawTexture(textures::leftArrow, (int)pos::paddleControlsLeftArrowPosX, (int)pos::leftAndRightArrowPosY, leftArrowButtonStatus);
			DrawTexture(textures::rightArrow, (int)pos::paddleControlsRightArrowPosX, (int)pos::leftAndRightArrowPosY, rightArrowButtonStatus);
			DrawTexture(textures::menuCircleButton, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::settingsCircleButton, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, settingsButtonStatus);
			DrawTexture(textures::defaultControls, (int)pos::paddleControlsDefaultPosX, (int)pos::downButtonsPosY, defaultControlsButtonStatus);

			DrawText("Change the controls", pos::paddleControlsText1PosX, pos::paddleControlsText1PosY, pos::textsSize5, BLACK);
			DrawText("Change the controls", pos::paddleControlsText2PosX, pos::paddleControlsText2PosY, pos::textsSize5, WHITE);
			DrawText("of your paddle", pos::paddleControlsText3PosX, pos::paddleControlsText3PosY, pos::textsSize5, BLACK);
			DrawText("of your paddle", pos::paddleControlsText4PosX, pos::paddleControlsText4PosY, pos::textsSize5, WHITE);

			DrawRectangleLinesEx({ pos::paddleControlsRec1PosX, pos::paddleControlsRecPosY, pos::paddleControlsrecSize, pos::paddleControlsrecSize }, (int)pos::recSizeLines, rec1Status);
			DrawRectangleLinesEx({ pos::paddleControlsRec2PosX, pos::paddleControlsRecPosY, pos::paddleControlsrecSize, pos::paddleControlsrecSize }, (int)pos::recSizeLines, rec2Status);

			showKey(key.leftPaddle, (int)pos::paddleControlsKey1PosX, (int)pos::paddleControlsKeysPosY);
			showKey(key.rightPaddle, (int)pos::paddleControlsKey2PosX, (int)pos::paddleControlsKeysPosY);

			if (rec1Active || rec2Active) DrawText("Press ENTER when you finish choosing the key", pos::paddleControlsText5PosX, pos::textsPosY3, pos::textsSize3, WHITE);
			else DrawText("Press the KEY you want to change", pos::paddleControlsText6PosX, pos::textsPosY3, pos::textsSize3, BLACK);

			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::defaultControls);
		}
	}
}