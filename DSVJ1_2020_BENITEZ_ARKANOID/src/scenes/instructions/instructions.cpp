#include "instructions.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace instructions
	{
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;
		
		void init()
		{			
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX,  pos::downButtonsPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::INSTRUCTIONS2;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::INSTRUCTIONS;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				menuButtonStatus = WHITE;
				rightArrowButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{		
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			
			if (screenWidth >= 1200)
			{
				DrawText("How to play arkanoid?", pos::instructionsPosX, pos::instructionsPosY, pos::textsSize5, BLACK);
				DrawText("How to play arkanoid?", pos::instructionsPosX2, pos::instructionsPosY2, pos::textsSize5, WHITE);
				DrawText("You have to move the bar to the sides in order to", pos::instructionsPosX3, pos::instructionsPosY3, pos::textsSize, WHITE);
				DrawText("catch the ball as itfalls and make it rise again.", pos::instructionsPosX4, pos::instructionsPosY4, pos::textsSize, WHITE);
				DrawText("The objective of this game is to break all the", pos::instructionsPosX5, pos::instructionsPosY5, pos::textsSize, WHITE);
				DrawText("bricks that are in the upper part.", pos::instructionsPosX6, pos::instructionsPosY6, pos::textsSize, WHITE);
				DrawText("You have five lives, if you can't pick up the!", pos::instructionsPosX7, pos::instructionsPosY7, pos::textsSize, WHITE);
				DrawText("ball in time you lose one.", pos::instructionsPosX8, pos::instructionsPosY8, pos::textsSize, WHITE);
				DrawText("If you manage to clear the screen, you will", pos::instructionsPosX9, pos::instructionsPosY9, pos::textsSize, WHITE);
				DrawText("pass the level, and so on.", pos::instructionsPosX10, pos::instructionsPosY10, pos::textsSize, WHITE);
			}
			else
			{
				DrawText("How to play arkanoid?", pos::instructionsPosX11, pos::instructionsPosY, pos::textsSize7, BLACK);
				DrawText("How to play arkanoid?", pos::instructionsPosX12, pos::instructionsPosY2, pos::textsSize7, WHITE);
				DrawText("You have to move the bar to the sides in order to", pos::instructionsPosX13, pos::instructionsPosY3, pos::textsSize3, WHITE);
				DrawText("catch the ball as itfalls and make it rise again.", pos::instructionsPosX14, pos::instructionsPosY4, pos::textsSize3, WHITE);
				DrawText("The objective of this game is to break all the", pos::instructionsPosX15, pos::instructionsPosY5, pos::textsSize3, WHITE);
				DrawText("bricks that are in the upper part.", pos::instructionsPosX16, pos::instructionsPosY6, pos::textsSize3, WHITE);
				DrawText("You have five lives, if you can't pick up the!", pos::instructionsPosX17, pos::instructionsPosY7, pos::textsSize3, WHITE);
				DrawText("ball in time you lose one.", pos::instructionsPosX18, pos::instructionsPosY8, pos::textsSize3, WHITE);
				DrawText("If you manage to clear the screen, you will", pos::instructionsPosX19, pos::instructionsPosY9, pos::textsSize3, WHITE);
				DrawText("pass the level, and so on.", pos::instructionsPosX20, pos::instructionsPosY10, pos::textsSize3, WHITE);
			}

			DrawTexture(textures::menuCircleButton, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::rightArrow, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, rightArrowButtonStatus);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
		}
	}
}