#ifndef AUDIO_H 
#define AUDIO_H

#include "raylib.h"

#include "scenes/gameplay/gameplay.h"

namespace audio
{
	struct GameSounds
	{
		Sound ballCollisionEntities;
		Sound ballCollisionWalls;
		Sound death;
		Sound powerup;
		Sound button;
		Sound winner;
		Sound loser;
	};

	struct GameMusic
	{
		Music menu;
		Music gameplay;
	};

	extern GameMusic gameMusic;
	extern float musicVolume;

	extern GameSounds gameSounds;
	extern float soundVolume;

	extern void menuAudio();
	extern void gameplayAudio();
	extern void resultAudio();

	extern void loadSounds();
	extern void setSoundsVolume();
	extern void unloadSounds();

	extern void loadMusic();
	extern void setMusicVolume();
	extern void unloadMusic();
}

#endif