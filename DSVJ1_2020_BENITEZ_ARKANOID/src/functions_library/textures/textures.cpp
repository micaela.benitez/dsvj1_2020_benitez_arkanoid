#include "textures.h"

namespace textures
{
	Texture2D backgroundGameplay;
	Texture2D backgroundMenu;
	Texture2D arkanoidTitle;
	Texture2D backgroundBorder;	
	Texture2D ballTexture;
	Texture2D blocksTexture;
	Texture2D unbreakableBlocks;
	Texture2D brokenBlocks;
	Texture2D brokenBlocks2;
	Texture2D lives;
	Texture2D arrows;
	Texture2D highScoreMenu;

	// Paddles
	Texture2D bluePaddleTexture;
	Texture2D violetPaddleTexture;
	Texture2D greenPaddleTexture;
	Texture2D redPaddleTexture;
	Texture2D pinkPaddleTexture;
	Texture2D orangePaddleTexture;
	Texture2D yellowPaddleTexture;
	Texture2D grayPaddleTexture;
	Texture2D blackPaddleTexture;

	// Long paddles
	Texture2D blueLongPaddleTexture;
	Texture2D violetLongPaddleTexture;
	Texture2D greenLongPaddleTexture;
	Texture2D redLongPaddleTexture;
	Texture2D pinkLongPaddleTexture;
	Texture2D orangeLongPaddleTexture;
	Texture2D yellowLongPaddleTexture;
	Texture2D grayLongPaddleTexture;
	Texture2D blackLongPaddleTexture;
	
	// Short paddles
	Texture2D blueShortPaddleTexture;
	Texture2D violetShortPaddleTexture;
	Texture2D greenShortPaddleTexture;
	Texture2D redShortPaddleTexture;
	Texture2D pinkShortPaddleTexture;
	Texture2D orangeShortPaddleTexture;
	Texture2D yellowShortPaddleTexture;
	Texture2D grayShortPaddleTexture;
	Texture2D blackShortPaddleTexture;

	// Buttons
	Texture2D playAgainButton;
	Texture2D backToMenuButton;
	Texture2D startButton;
	Texture2D instructionsButton;
	Texture2D settingsButton;
	Texture2D creditsButton;
	Texture2D exitButton;
	Texture2D menuCircleButton;
	Texture2D settingsCircleButton;
	Texture2D exitCircleButton;
	Texture2D rightArrow;
	Texture2D leftArrow;
	Texture2D paddleControls;
	Texture2D paddleColor;
	Texture2D ballColor;
	Texture2D defaultControls;
	Texture2D nextLevel;
	Texture2D startOver;
	Texture2D configurations;
	Texture2D resolution;
	Texture2D quit;
	Texture2D gameAudio;

	// Powerups
	Texture2D multiballPowerup;
	Texture2D fireballPowerup;
	Texture2D paddlePowerup;
	Texture2D ballPowerup;
}