#include "menu.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace menu
	{
		static Color startButtonStatus = WHITE;
		static Color instructionsButtonStatus = WHITE;
		static Color settingsButtonStatus = WHITE;
		static Color creditsButtonStatus = WHITE;
		static Color exitButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;
		
		void init()
		{			
			textures::backgroundMenu = LoadTexture("res/assets/textures/backgroundmenu.png");
			textures::backgroundMenu.width = (int)screenWidth;
			textures::backgroundMenu.height = (int)screenHeight;

			textures::arkanoidTitle = LoadTexture("res/assets/textures/title.png");

			textures::startButton = LoadTexture("res/raw/textures/start.png");
			textures::startButton.width = sizes::buttonsWidth;
			textures::startButton.height = sizes::buttonsHeight;

			textures::instructionsButton = LoadTexture("res/raw/textures/instructions.png");
			textures::instructionsButton.width = sizes::buttonsWidth;
			textures::instructionsButton.height = sizes::buttonsHeight;

			textures::settingsButton = LoadTexture("res/raw/textures/settings.png");
			textures::settingsButton.width = sizes::buttonsWidth;
			textures::settingsButton.height = sizes::buttonsHeight;

			textures::creditsButton = LoadTexture("res/raw/textures/credits.png");
			textures::creditsButton.width = sizes::buttonsWidth;
			textures::creditsButton.height = sizes::buttonsHeight;

			textures::exitButton = LoadTexture("res/raw/textures/exit.png");
			textures::exitButton.width = sizes::buttonsWidth;
			textures::exitButton.height = sizes::buttonsHeight;

			textures::highScoreMenu = LoadTexture("res/raw/textures/highscoremenu.png");
			textures::highScoreMenu.width = sizes::highScoreSize;
			textures::highScoreMenu.height = sizes::highScoreSize;

			textures::resolution = LoadTexture("res/raw/textures/resolution.png");
			textures::resolution.width = sizes::sizeCircleButtons;
			textures::resolution.height = sizes::sizeCircleButtons;

			textures::gameAudio = LoadTexture("res/raw/textures/audio.png");
			textures::gameAudio.width = sizes::sizeCircleButtons;
			textures::gameAudio.height = sizes::sizeCircleButtons;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::buttonPosY, (float)textures::startButton.width, (float)textures::startButton.height }))
			{
				startButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::GAMEPLAY;
					StopMusicStream(audio::gameMusic.menu);
					gameplay::initialize();
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::button2PosY, (float)textures::instructionsButton.width, (float)textures::instructionsButton.height }))
			{
				instructionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::INSTRUCTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::button3PosY, (float)textures::settingsButton.width, (float)textures::settingsButton.height }))
			{
				settingsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::SETTINGS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::button4PosY, (float)textures::creditsButton.width, (float)textures::creditsButton.height }))
			{
				creditsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::CREDITS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::button5PosY, (float)textures::exitButton.width, (float)textures::exitButton.height }))
			{
				exitButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::exitButton = true;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::MENU;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::MENU;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				startButtonStatus = WHITE;
				instructionsButtonStatus = WHITE;
				settingsButtonStatus = WHITE;
				creditsButtonStatus = WHITE;
				exitButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawTexture(textures::arkanoidTitle, (int)pos::menuTitlePosX, (int)pos::menuTitlePosY, WHITE);
			DrawRectangle((int)pos::rec1PosX, (int)pos::recPosY, (int)pos::menuRecWidth, (int)pos::recSizeLines, BLACK);
			DrawRectangle((int)pos::menuRec2PosX, (int)pos::recPosY, (int)pos::menuRecWidth, (int)pos::recSizeLines, BLACK);
			DrawTexture(textures::highScoreMenu, (int)pos::menuHighScorePosX, (int)pos::menuHighScorePosY, WHITE);
			DrawText("V 2.0", pos::versionPosX, pos::versionPosY, pos::textsSize3, WHITE);

			if (highScore > 0)
			{
				DrawText("High", pos::menuPosX, pos::menuPosY, pos::textsSize3, BLACK);
				DrawText("Score", pos::menuPosX2, pos::menuPosY2, pos::textsSize3, BLACK);
				DrawText(TextFormat("%06i", highScore), pos::menuPosX3, pos::menuPosY3, pos::textsSize3, BLACK);
			}
			else
			{
				DrawText("You dont", pos::menuPosX4, pos::menuPosY, pos::textsSize3, BLACK);
				DrawText("have a", pos::menuPosX5, pos::menuPosY2, pos::textsSize3, BLACK);
				DrawText("high score", pos::menuPosX6, pos::menuPosY4, pos::textsSize3, BLACK);
				DrawText("yet", pos::menuPosX7, pos::menuPosY5, pos::textsSize3, BLACK);
			}

			// Buttons			
			DrawTexture(textures::startButton, (int)pos::buttonsPosX, (int)pos::buttonPosY, startButtonStatus);
			DrawTexture(textures::instructionsButton, (int)pos::buttonsPosX, (int)pos::button2PosY, instructionsButtonStatus);
			DrawTexture(textures::settingsButton, (int)pos::buttonsPosX, (int)pos::button3PosY, settingsButtonStatus);
			DrawTexture(textures::creditsButton, (int)pos::buttonsPosX, (int)pos::button4PosY, creditsButtonStatus);
			DrawTexture(textures::exitButton, (int)pos::buttonsPosX, (int)pos::button5PosY, exitButtonStatus);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::backgroundMenu);
			UnloadTexture(textures::arkanoidTitle);
			UnloadTexture(textures::startButton);
			UnloadTexture(textures::instructionsButton);
			UnloadTexture(textures::settingsButton);
			UnloadTexture(textures::creditsButton);
			UnloadTexture(textures::exitButton);
			UnloadTexture(textures::highScoreMenu);
			UnloadTexture(textures::resolution);
			UnloadTexture(textures::gameAudio);
		}
	}
}