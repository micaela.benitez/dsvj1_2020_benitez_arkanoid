#ifndef INSTRUCTIONS4_H
#define INSTRUCTIONS4_H

#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace instructions4
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif