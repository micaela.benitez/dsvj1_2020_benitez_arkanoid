#include "keys.h"

using namespace pong;
using namespace paddle_controls;
using namespace gameplay;

void changeKey(int& key, int key2, int& numberKey)
{       
    static const int firstKey = 0;
    static const int lastKey = 27;

    if (CheckCollisionPointRec(mousePoint, { pos::paddleControlsLeftArrowPosX, pos::leftAndRightArrowPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
    {        
        leftArrowButtonStatus = GRAY;
        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
        {
            sounds.button = true;
            numberKey--;
        }
    }
    else if (CheckCollisionPointRec(mousePoint, { pos::paddleControlsRightArrowPosX,  pos::leftAndRightArrowPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
    {
        rightArrowButtonStatus = GRAY;
        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
        {
            sounds.button = true;
            numberKey++;
        }
    }
    else
    {
        leftArrowButtonStatus = WHITE;
        rightArrowButtonStatus = WHITE;
    }

    if (numberKey < firstKey) numberKey = lastKey;
    else if (numberKey > lastKey) numberKey = firstKey;

    if (key == key2 && CheckCollisionPointRec(mousePoint, { pos::paddleControlsLeftArrowPosX, pos::leftAndRightArrowPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height })) numberKey--;
    else if (key == key2 && CheckCollisionPointRec(mousePoint, { pos::paddleControlsRightArrowPosX,  pos::leftAndRightArrowPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height })) numberKey++;
    
    if (numberKey == 0) key = KEY_A;
    else if (numberKey == 1) key = KEY_B;
    else if (numberKey == 2) key = KEY_C;
    else if (numberKey == 3) key = KEY_D;
    else if (numberKey == 4) key = KEY_E;
    else if (numberKey == 5) key = KEY_F;
    else if (numberKey == 6) key = KEY_G;
    else if (numberKey == 7) key = KEY_H;
    else if (numberKey == 8) key = KEY_I;
    else if (numberKey == 9) key = KEY_J;
    else if (numberKey == 10) key = KEY_K;
    else if (numberKey == 11) key = KEY_L;
    else if (numberKey == 12) key = KEY_M;
    else if (numberKey == 13) key = KEY_N;
    else if (numberKey == 14) key = KEY_O;
    else if (numberKey == 15) key = KEY_P;
    else if (numberKey == 16) key = KEY_Q;
    else if (numberKey == 17) key = KEY_R;
    else if (numberKey == 18) key = KEY_S;
    else if (numberKey == 19) key = KEY_T;
    else if (numberKey == 20) key = KEY_U;
    else if (numberKey == 21) key = KEY_V;
    else if (numberKey == 22) key = KEY_W;
    else if (numberKey == 23) key = KEY_X;
    else if (numberKey == 24) key = KEY_Y;
    else if (numberKey == 25) key = KEY_Z;
    else if (numberKey == 26) key = KEY_LEFT;
    else if (numberKey == 27) key = KEY_RIGHT;
}

void showKey(int key, int posX, int posY)
{
    if (key == KEY_A) DrawText("A", posX, posY, pos::keysSize , BLACK);
    else if (key == KEY_B) DrawText("B", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_C) DrawText("C", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_D) DrawText("D", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_E) DrawText("E", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_F) DrawText("F", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_G) DrawText("G", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_H) DrawText("H", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_I) DrawText("I", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_J) DrawText("J", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_K) DrawText("K", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_L) DrawText("L", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_M) DrawText("M", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_N) DrawText("N", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_O) DrawText("O", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_P) DrawText("P", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_Q) DrawText("Q", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_R) DrawText("R", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_S) DrawText("S", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_T) DrawText("T", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_U) DrawText("U", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_V) DrawText("V", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_W) DrawText("W", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_X) DrawText("X", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_Y) DrawText("Y", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_Z) DrawText("Z", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_LEFT) DrawText("<-", posX, posY, pos::keysSize, BLACK);
    else if (key == KEY_RIGHT) DrawText("->", posX, posY, pos::keysSize, BLACK);
}