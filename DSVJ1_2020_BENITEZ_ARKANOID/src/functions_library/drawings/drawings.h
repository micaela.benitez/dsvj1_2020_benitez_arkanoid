#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "functions_library/entities/ball.h"
#include "functions_library/entities/paddle.h"
#include "functions_library/entities/block.h"
#include "scenes/gameplay/gameplay.h"

void drawPaddle();
void drawBall(Ball& ball);
void drawScreenDetails();
void drawPauseOptions();
void drawPowerups();
void drawTimerFireballPowerup(Ball& ball);

#endif