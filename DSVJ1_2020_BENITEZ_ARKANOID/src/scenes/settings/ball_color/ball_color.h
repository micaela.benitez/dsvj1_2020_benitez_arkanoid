#ifndef BALL_COLOR_H
#define BALL_COLOR_H

#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace ball_color
	{
		extern Color ballColor;

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif