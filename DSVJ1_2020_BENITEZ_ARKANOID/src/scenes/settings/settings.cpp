#include "settings.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace settings
	{
		static Color paddleControlsButtonStatus = WHITE;
		static Color paddleColorButtonStatus = WHITE;
		static Color ballColorButtonStatus = WHITE;
		static Color configurationsButtonStatus = WHITE;
		static Color backToMenuButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;
		
		void init()
		{
			textures::paddleControls = LoadTexture("res/raw/textures/paddlecontrols.png");
			textures::paddleControls.width = sizes::buttonsWidth;
			textures::paddleControls.height = sizes::buttonsHeight;

			textures::paddleColor = LoadTexture("res/raw/textures/paddlecolor.png");
			textures::paddleColor.width = sizes::buttonsWidth;
			textures::paddleColor.height = sizes::buttonsHeight;

			textures::ballColor = LoadTexture("res/raw/textures/ballcolor.png");
			textures::ballColor.width = sizes::buttonsWidth;
			textures::ballColor.height = sizes::buttonsHeight;

			textures::configurations = LoadTexture("res/raw/textures/configurations.png");
			textures::configurations.width = sizes::buttonsWidth;
			textures::configurations.height = sizes::buttonsHeight;

			textures::settingsCircleButton = LoadTexture("res/raw/textures/settingscircle.png");
			textures::settingsCircleButton.width = sizes::sizeCircleButtons;
			textures::settingsCircleButton.height = sizes::sizeCircleButtons;

			textures::arrows = LoadTexture("res/assets/textures/arrows.png");
			textures::arrows.width = sizes::arrowsWidth;
			textures::arrows.height = sizes::arrowsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::buttonPosY, (float)textures::paddleControls.width, (float)textures::paddleControls.height }))
			{
				paddleControlsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::PADDLECONTROLS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::button2PosY, (float)textures::paddleColor.width, (float)textures::paddleColor.height }))
			{
				paddleColorButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::PADDLECOLOR;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX, pos::button3PosY, (float)textures::ballColor.width, (float)textures::ballColor.height }))
			{
				ballColorButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::BALLCOLOR;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX,  pos::button4PosY, (float)textures::configurations.width, (float)textures::configurations.height }))
			{
				configurationsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::CONFIGURATIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsPosX,  pos::button5PosY, (float)textures::backToMenuButton.width, (float)textures::backToMenuButton.height }))
			{
				backToMenuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::SETTINGS;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::SETTINGS;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				paddleControlsButtonStatus = WHITE;
				paddleColorButtonStatus = WHITE;
				ballColorButtonStatus = WHITE;
				configurationsButtonStatus = WHITE;
				backToMenuButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawTexture(textures::paddleControls, (int)pos::buttonsPosX, (int)pos::buttonPosY, paddleControlsButtonStatus);
			DrawTexture(textures::paddleColor, (int)pos::buttonsPosX, (int)pos::button2PosY, paddleColorButtonStatus);
			DrawTexture(textures::ballColor, (int)pos::buttonsPosX, (int)pos::button3PosY, ballColorButtonStatus);
			DrawTexture(textures::configurations, (int)pos::buttonsPosX, (int)pos::button4PosY, configurationsButtonStatus);
			DrawTexture(textures::backToMenuButton, (int)pos::buttonsPosX, (int)pos::button5PosY, backToMenuButtonStatus);

			DrawText("Select an option", pos::settingsTextPosX, pos::settingsTextPosY, pos::textsSize5, BLACK);
			DrawText("Select an option", pos::settingsTextPosX2, pos::settingsTextPosY2, pos::textsSize5, WHITE);
			DrawRectangle(pos::rec1PosX, pos::recPosY, (int)pos::settingRecWidth, (int)pos::recSizeLines, BLACK);
			DrawRectangle(pos::settingRec2PosX, pos::recPosY, (int)pos::settingRecWidth, (int)pos::recSizeLines, BLACK);

			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::paddleControls);
			UnloadTexture(textures::paddleColor);
			UnloadTexture(textures::ballColor);
			UnloadTexture(textures::configurations);
			UnloadTexture(textures::settingsCircleButton);
			UnloadTexture(textures::arrows);
		}
	}
}