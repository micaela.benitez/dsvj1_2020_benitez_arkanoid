#ifndef ENTITIES_MOTION_H
#define ENTITIES_MOTION_H

#include <cmath>
#include <ctime>
#include <cstdlib>

#include "scenes/gameplay/gameplay.h"

void paddleMotion();
void ballMotion();
void ballMotion(Ball& ball);
void ballLoss();
void ballVsBlocks(Ball& ball);

#endif