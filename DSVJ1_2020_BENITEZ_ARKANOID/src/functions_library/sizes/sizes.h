#ifndef SIZES_H
#define SIZES_H

#include "scenes/gameplay/gameplay.h"

namespace sizes
{
	extern float blocksSizeX;
	extern float blocksSizeY;

	extern int livesWidth;
	extern int livesHeight;

	extern int buttonsWidth;
	extern int buttonsHeight;
	
	extern int sizeCircleButtons;

	extern int arrowsWidth;
	extern int arrowsHeight;

	extern int highScoreSize;

	void updateSizes();
}

#endif
