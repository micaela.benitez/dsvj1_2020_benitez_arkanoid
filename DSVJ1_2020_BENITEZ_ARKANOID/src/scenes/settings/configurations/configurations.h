#ifndef CONFIGURATIONS_H
#define CONFIGURATIONS_H

#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace configurations
	{
		extern bool powerupsConfigurationActive;

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif