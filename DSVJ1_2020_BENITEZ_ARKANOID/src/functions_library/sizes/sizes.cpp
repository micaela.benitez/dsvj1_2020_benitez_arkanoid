#include "sizes.h"

using namespace pong;
using namespace gameplay;

namespace sizes
{
	float blocksSizeX = ((screenWidth - borderSize * 2) / blocksPerColumn);
	float blocksSizeY = 60;

	int livesWidth = 40;
	int livesHeight = 30;

	int buttonsWidth = 300;
	int buttonsHeight = 80;

	int sizeCircleButtons = 80;

	int arrowsWidth = 900;
	int arrowsHeight = 500;

	int highScoreSize = 200;

	void updateSizes()
	{
		blocksSizeX = ((screenWidth - borderSize * 2) / blocksPerColumn);		
	
		blocksSizeY = 60;

		livesWidth = 40;
		livesHeight = 30;

		buttonsWidth = 300;
		buttonsHeight = 80;

		sizeCircleButtons = 80;

		arrowsWidth = 900;
		arrowsHeight = 500;

		highScoreSize = 200;
	}
}