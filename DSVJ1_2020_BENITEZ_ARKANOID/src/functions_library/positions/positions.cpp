#include "positions.h"

using namespace pong;
using namespace gameplay;


namespace pos
{	
	float backgroundPos = 0;

	float recSizeLines = 5;
	float recHeight = 75;

	float leftDownButtonPosX = 40;
	float centerDownButtonPosX = (screenWidth / 2) - 40;
	float rightDownButtonsPosX = screenWidth - 120;
	float downButtonsPosY = screenHeight - 120;

	float arrowsPosX = (screenWidth / 2) - 470;
	float arrowsPosY = (screenHeight / 2) - 210;

	float leftArrowPosX = 80;
	float rightArrowPosX = screenWidth - 170;
	float leftAndRightArrowPosY = screenHeight / 2;

	float paddlePosX = (screenWidth / 2) - 70;
	float ballPosX = (screenWidth / 2) - 20;
	float entitiesPosY = (screenHeight / 2) + 30;

	float buttonsUpLeftPosX = 5;
	float buttonsUpRightPosX = screenWidth - 85;
	float buttonsUpPosY = 0;

	float pauseAudioButtonPosX = (screenWidth / 2) - 40;
	float pauseAudioButtonPosY = (screenHeight / 2) + 250;

	float paddleControlsLeftArrowPosX = 100;
	float paddleControlsRightArrowPosX = screenWidth - 190;
	float paddleControlsDefaultPosX = (screenWidth / 2) - 150;
	float paddleControlsRec1PosX = (screenWidth / 2) - 150;
	float paddleControlsRec2PosX = (screenWidth / 2) + 50;
	float paddleControlsKey1PosX = (screenWidth / 2) - 116;
	float paddleControlsKey2PosX = (screenWidth / 2) + 82;
	float paddleControlsRecPosY = (screenHeight / 2) - 10;
	float paddleControlsKeysPosY = (screenHeight / 2) + 16;
	float paddleControlsrecSize = 100;

	float resultLeftButtons = (screenWidth / 2) - 380;
	float resultRightButtons = (screenWidth / 2) + 90;
	float resultCenterButton = (screenWidth / 2) - 150;
	float resultUpButtons = (screenHeight / 2) + 80;
	float resultDownButtons = (screenHeight / 2) + 230;

	float instructions2Block1PosX = (screenWidth / 2) - 330;
	float instructions2Block2PosX = (screenWidth / 2) - 65;
	float instructions2Block3PosX = (screenWidth / 2) + 205;
	float instructions2Block1PosX2 = (screenWidth / 2) - 450;
	float instructions2Block3PosX2 = (screenWidth / 2) + 325;
	float instructions2Block1PosY = 320;
	float instructions2Block2PosY = 390;
	float instructions2Block3PosY = 460;
	float instructions2Rec1PosX = (screenWidth / 2) - 200;
	float instructions2Rec2PosX = (screenWidth / 2) + 130;
	float instructions2Rec1PosX2 = (screenWidth / 2) - 220;
	float instructions2Rec2PosX2 = (screenWidth / 2) + 150;
	float instructions2RecPosY = (screenHeight / 2) + 150;
	float instructions2RecPosX = (screenWidth / 2) - 70;
	float instructions2Rec1PosY = 550;
	float instructions2Rec2PosY = 630;

	float instructions3PowerupPosX = (screenWidth / 2) - 30;
	float instructions3PowerupPosY = (screenHeight / 2) - 240;
	float instructions3PowerupPosY2 = (screenHeight / 2) + 80;
	float instructions4PowerupPosY3 = (screenHeight / 2) + 40;

	float configurationsRecPosX = (screenWidth / 2) - 270;
	float configurationsRecPosX2 = (screenWidth / 2) - 80;
	float configurationsRecPosX3 = (screenWidth / 2) + 140;
	float configurationsRecPosX4 = (screenWidth / 2) - 198;
	float configurationsRecPosX5 = (screenWidth / 2) - 28;
	float configurationsRecPosX6 = (screenWidth / 2) + 138;
	float configurationsRecPosX7 = (screenWidth / 2) - 120;
	float configurationsRecPosX8 = (screenWidth / 2) + 20;
	float configurationsRecPosY = (screenHeight / 2) - 140;
	float configurationsRecPosY2 = (screenHeight / 2) + 40;
	float configurationsRecPosY3 = (screenHeight / 2) + 220;
	float configurationsRecWidth = 140;
	float configurationsRecWidth2 = 170;
	float configurationsRecWidth3 = 60;
	float configurationsRecWidth4 = 100;

	float resolutionsRecPosX = (screenWidth / 2) - 190;
	float resolutionsRecPosX2 = (screenWidth / 2) - 50;
	float resolutionsRecPosX3 = (screenWidth / 2) + 90;
	float resolutionsRecPosX4 = (screenWidth / 2) - 190;
	float resolutionsRecPosX5 = (screenWidth / 2) - 50;
	float resolutionsRecPosX6 = (screenWidth / 2) + 90;
	float resolutionsRecPosY = (screenHeight / 2) - 60;
	float resolutionsRecPosY2 = (screenHeight / 2) + 150;
	float resolutionsRecWidth = 110;

	float audioRecPosX = (screenWidth / 2) - 250;
	float audioRecPosX2 = (screenWidth / 2) - 65;
	float audioRecPosX3 = (screenWidth / 2) + 120;
	float audioRecPosX4 = (screenWidth / 2) - 250;
	float audioRecPosX5 = (screenWidth / 2) - 65;
	float audioRecPosX6 = (screenWidth / 2) + 120;
	float audioRecPosY = (screenHeight / 2) - 50;
	float audioRecPosY2 = (screenHeight / 2) + 130;
	float audioRecWidth = 135;

	float creditsRec1PosX = 50;
	float creditsRec2PosX = (screenWidth / 2) + 185;
	float creditsRecPosY = (screenHeight / 2) - 280;
	float creditsRecSize = (screenWidth / 2) - 240;

	float menuRecWidth = (screenWidth / 2) - 350;
	float menuRec2PosX = (screenWidth / 2) + 290;
	float menuTitlePosX = (screenWidth / 2) - 250;
	float menuTitlePosY = (screenHeight / 2) - 380;
	float menuHighScorePosX = screenWidth - 270;
	float menuHighScorePosY = (screenHeight / 2) - 100;

	float settingRecWidth = (screenWidth / 2) - 380;
	float settingRec2PosX = (screenWidth / 2) + 330;

	float rec1PosX = 50;
	float recPosY = (screenHeight / 2) - 280;
	float buttonsPosX = (screenWidth / 2) - 150;
	float buttonPosY = (screenHeight / 2) - 180;
	float button2PosY = (screenHeight / 2) - 70;
	float button3PosY = (screenHeight / 2) + 40;
	float button4PosY = (screenHeight / 2) + 150;
	float button5PosY = (screenHeight / 2) + 260;

	// Texts
	int actualScorePosX = 100;
	int actualScorePosX2 = 170;
	int livesPosX = (screenWidth / 2) - 50;
	int livesPosX2 = (screenWidth / 2) - 140;
	int livesPosX3 = (screenWidth / 2) - 100;
	int livesPosX4 = (screenWidth / 2) - 60;
	int livesPosX5 = (screenWidth / 2) - 20;
	int livesPosX6 = (screenWidth / 2) + 20;
	int livesPosX7 = (screenWidth / 2) + 60;
	int livesPosX8 = (screenWidth / 2) + 100;
	int highScorePosX = screenWidth - 330;
	int highScorePosX2 = screenWidth - 290;
	int pausePosX = (screenWidth / 2) - 170;
	int enterPosX = (screenWidth / 2) - 240;
	int spacebarPosX = (screenWidth / 2) - 290;
	int paddleControlsText1PosX = (screenWidth / 2) - 370;
	int paddleControlsText2PosX = (screenWidth / 2) - 375;
	int paddleControlsText3PosX = (screenWidth / 2) - 260;
	int paddleControlsText4PosX = (screenWidth / 2) - 265;
	int paddleControlsText5PosX = (screenWidth / 2) - 365;
	int paddleControlsText6PosX = (screenWidth / 2) - 270;
	int settingsTextPosX = (screenWidth / 2) - 290;
	int settingsTextPosX2 = (screenWidth / 2) - 295;
	int paddleColorTextPosX = (screenWidth / 2) - 530;
	int paddleColorTextPosX2 = (screenWidth / 2) - 535;
	int paddleColorTextPosX3 = (screenWidth / 2) - 330;
	int paddleColorTextPosX4 = (screenWidth / 2) - 335;
	int paddleColorTextPosX5 = (screenWidth / 2) - 185;
	int paddleColorTextPosX6 = (screenWidth / 2) - 190;
	int ballColorTextPosX = (screenWidth / 2) - 480;
	int ballColorTextPosX2 = (screenWidth / 2) - 485;
	int ballColorTextPosX3 = (screenWidth / 2) - 330;
	int ballColorTextPosX4 = (screenWidth / 2) - 335;
	int ballColorTextPosX5 = (screenWidth / 2) - 130;
	int ballColorTextPosX6 = (screenWidth / 2) - 135;
	int configurationsTextPosX = (screenWidth / 2) - 260;
	int configurationsTextPosX2 = (screenWidth / 2) - 265;
	int gameOverPosX = (screenWidth / 2) - 250;
	int resultPosX = (screenWidth / 2) - 200;
	int creditsPosX = (screenWidth / 2) - 125;
	int creditsPosX2 = (screenWidth / 2) - 130;
	int creditsPosX3 = (screenWidth / 2) - 115;
	int creditsPosX4 = (screenWidth / 2) - 150;
	int creditsPosX5 = (screenWidth / 2) - 100;
	int creditsPosX6 = (screenWidth / 2) - 150;
	int creditsPosX7 = (screenWidth / 2) - 205;
	int creditsPosX8 = (screenWidth / 2) - 245;
	int creditsPosX9 = (screenWidth / 2) - 80;
	int creditsPosX10 = (screenWidth / 2) - 75;
	int creditsPosX11 = (screenWidth / 2) - 80;
	int credits2PosX = (screenWidth / 2) - 50;
	int credits2PosX2 = (screenWidth / 2) - 215;
	int credits2PosX3 = (screenWidth / 2) - 275;
	int credits2PosX4 = (screenWidth / 2) - 220;
	int credits2PosX5 = (screenWidth / 2) - 225;
	int credits2PosX6 = (screenWidth / 2) - 230;
	int credits2PosX7 = (screenWidth / 2) - 280;
	int instructionsPosX = (screenWidth / 2) - 390;
	int instructionsPosX2 = (screenWidth / 2) - 395;
	int instructionsPosX3 = (screenWidth / 2) - 520;
	int instructionsPosX4 = (screenWidth / 2) - 480;
	int instructionsPosX5 = (screenWidth / 2) - 470;
	int instructionsPosX6 = (screenWidth / 2) - 350;
	int instructionsPosX7 = (screenWidth / 2) - 450;
	int instructionsPosX8 = (screenWidth / 2) - 255;
	int instructionsPosX9 = (screenWidth / 2) - 450;
	int instructionsPosX10 = (screenWidth / 2) - 255;
	int instructionsPosX11 = (screenWidth / 2) - 335;
	int instructionsPosX12 = (screenWidth / 2) - 340;
	int instructionsPosX13 = (screenWidth / 2) - 390;
	int instructionsPosX14 = (screenWidth / 2) - 360;
	int instructionsPosX15 = (screenWidth / 2) - 350;
	int instructionsPosX16 = (screenWidth / 2) - 260;
	int instructionsPosX17 = (screenWidth / 2) - 340;
	int instructionsPosX18 = (screenWidth / 2) - 185;
	int instructionsPosX19 = (screenWidth / 2) - 340;
	int instructionsPosX20 = (screenWidth / 2) - 185;
	int instructions2PosX = (screenWidth / 2) - 320;
	int instructions2PosX2 = (screenWidth / 2) - 325;
	int instructions2PosX3 = (screenWidth / 2) - 350;
	int instructions2PosX4 = (screenWidth / 2) - 360;
	int instructions2PosX5 = (screenWidth / 2) - 180;
	int instructions2PosX6 = (screenWidth / 2) + 150;
	int instructions2PosX7 = (screenWidth / 2) - 500;
	int instructions2PosX8 = (screenWidth / 2) - 460;
	int instructions2PosX9 = (screenWidth / 2) - 200;
	int instructions2PosX10 = (screenWidth / 2) + 170;
	int instructions3PosX = (screenWidth / 2) - 165;
	int instructions3PosX2 = (screenWidth / 2) - 170;
	int instructions3PosX3 = (screenWidth / 2) - 100;
	int instructions3PosX4 = (screenWidth / 2) - 330;
	int instructions3PosX5 = (screenWidth / 2) - 340;
	int instructions3PosX6 = (screenWidth / 2) - 335;
	int instructions3PosX7 = (screenWidth / 2) - 90;
	int instructions3PosX8 = (screenWidth / 2) - 300;
	int instructions3PosX9 = (screenWidth / 2) - 180;
	int instructions3PosX10 = (screenWidth / 2) - 170;
	int instructions3PosX11 = (screenWidth / 2) - 510;
	int instructions3PosX12 = (screenWidth / 2) - 520;
	int instructions3PosX13 = (screenWidth / 2) - 515;
	int instructions3PosX14 = (screenWidth / 2) - 160;
	int instructions3PosX15 = (screenWidth / 2) - 480;
	int instructions3PosX16 = (screenWidth / 2) - 280;
	int instructions4PosX = (screenWidth / 2) - 90;
	int instructions4PosX2 = (screenWidth / 2) - 330;
	int instructions4PosX3 = (screenWidth / 2) - 140;
	int instructions4PosX4 = (screenWidth / 2) - 80;
	int instructions4PosX5 = (screenWidth / 2) - 500;
	int instructions4PosX6 = (screenWidth / 2) - 220;
	int instructions4PosX7 = (screenWidth / 2) - 120;
	int instructions4PosX8 = (screenWidth / 2) - 115;
	int menuPosX = screenWidth - 200;
	int menuPosX2 = screenWidth - 217;
	int menuPosX3 = screenWidth - 225;
	int menuPosX4 = screenWidth - 235;
	int menuPosX5 = screenWidth - 217;
	int menuPosX6 = screenWidth - 250;
	int menuPosX7 = screenWidth - 190;
	int configurationsPosX = (screenWidth / 2) - 220;
	int configurationsPosX2 = (screenWidth / 2) - 250;
	int configurationsPosX3 = (screenWidth / 2) - 60;
	int configurationsPosX4 = (screenWidth / 2) + 160;
	int configurationsPosX5 = (screenWidth / 2) - 50;
	int configurationsPosX6 = (screenWidth / 2) - 178;
	int configurationsPosX7 = (screenWidth / 2) - 8;
	int configurationsPosX8 = (screenWidth / 2) + 158;
	int configurationsPosX9 = (screenWidth / 2) - 100;
	int configurationsPosX10 = (screenWidth / 2) - 105;
	int configurationsPosX11 = (screenWidth / 2) + 45;
	int resolutionsPosX = (screenWidth / 2) - 180;
	int resolutionsPosX2 = (screenWidth / 2) - 185;
	int resolutionsPosX3 = (screenWidth / 2) - 50;
	int resolutionsPosX4 = (screenWidth / 2) - 170;
	int resolutionsPosX5 = (screenWidth / 2) - 35;
	int resolutionsPosX6 = (screenWidth / 2) + 105;
	int resolutionsPosX7 = (screenWidth / 2) - 60;
	int resolutionsPosX8 = (screenWidth / 2) - 170;
	int resolutionsPosX9 = (screenWidth / 2) - 30;
	int resolutionsPosX10 = (screenWidth / 2) + 105;
	int audioPosX = (screenWidth / 2) - 90;
	int audioPosX2 = (screenWidth / 2) - 95;
	int audioPosX3 = (screenWidth / 2) - 55;
	int audioPosX4 = (screenWidth / 2) - 230;
	int audioPosX5 = (screenWidth / 2) - 30;
	int audioPosX6 = (screenWidth / 2) + 145;
	int audioPosX7 = (screenWidth / 2) - 70;
	int audioPosX8 = (screenWidth / 2) - 230;
	int audioPosX9 = (screenWidth / 2) - 30;
	int audioPosX10 = (screenWidth / 2) + 145;
	int versionPosX = 20;
	int versionPosX2 = (screenWidth / 2) - 110;

	int textsPosY = 0;
	int textsPosY2 = 40;
	int textsPosY3 = (screenHeight / 2) + 150;
	int textsPosY4 = (screenHeight / 2) + 200;
	int paddleControlsText1PosY = 80;
	int paddleControlsText2PosY = 85;
	int paddleControlsText3PosY = 170;
	int paddleControlsText4PosY = 175;
	int settingsTextPosY = (screenHeight / 2) - 310;
	int settingsTextPosY2 = (screenHeight / 2) - 315;
	int colorTextPosY = 90;
	int colorTextPosY2 = 95;
	int colorTextPosY3 = 80;
	int colorTextPosY4 = 85;
	int colorTextPosY5 = 170;
	int colorTextPosY6 = 175;
	int gameOverPosY = (screenHeight / 2) - 210;
	int resultPosY = (screenHeight / 2) - 70;
	int creditsPosY = (screenHeight / 2) - 310;
	int creditsPosY2 = (screenHeight / 2) - 315;
	int creditsPosY3 = (screenHeight / 2) - 170;
	int creditsPosY4 = (screenHeight / 2) - 120;
	int creditsPosY5 = (screenHeight / 2) - 40;
	int creditsPosY6 = (screenHeight / 2) + 10;
	int creditsPosY7 = (screenHeight / 2) + 60;
	int creditsPosY8 = (screenHeight / 2) + 110;
	int creditsPosY9 = (screenHeight / 2) + 160;
	int creditsPosY10 = (screenHeight / 2) + 210;
	int creditsPosY11 = (screenHeight / 2) + 260;
	int credits2PosY = (screenHeight / 2) - 120;
	int credits2PosY2 = (screenHeight / 2) - 70;
	int credits2PosY3 = (screenHeight / 2) - 20;
	int credits2PosY4 = (screenHeight / 2) + 30;
	int credits2PosY5 = (screenHeight / 2) + 80;
	int credits2PosY6 = (screenHeight / 2) + 130;
	int credits2PosY7 = (screenHeight / 2) + 180;
	int instructionsPosY = 70;
	int instructionsPosY2 = 75;
	int instructionsPosY3 = 220;
	int instructionsPosY4 = 270;
	int instructionsPosY5 = 370;
	int instructionsPosY6 = 420;
	int instructionsPosY7 = 520;
	int instructionsPosY8 = 570;
	int instructionsPosY9 = 670;
	int instructionsPosY10 = 720;
	int instructions2PosY = (screenHeight / 2) + 166;
	int instructions2PosY2 = (screenHeight / 2) + 200;
	int instructions2PosY3 = (screenHeight / 2) + 166;
	int instructions3PosY = (screenHeight / 2) - 190;
	int instructions3PosY2 = (screenHeight / 2) - 140;
	int instructions3PosY3 = (screenHeight / 2) - 90;
	int instructions3PosY4 = (screenHeight / 2) - 40;
	int instructions3PosY5 = (screenHeight / 2) + 140;
	int instructions3PosY6 = (screenHeight / 2) + 180;
	int instructions3PosY7 = (screenHeight / 2) + 230;
	int instructions4PosY = (screenHeight / 2) - 190;
	int instructions4PosY2 = (screenHeight / 2) - 140;
	int instructions4PosY3 = (screenHeight / 2) - 90;
	int instructions4PosY4 = (screenHeight / 2) + 90;
	int instructions4PosY5 = (screenHeight / 2) + 140;
	int instructions4PosY6 = (screenHeight / 2) + 190;
	int instructions4PosY7 = screenHeight - 100;
	int instructions4PosY8 = screenHeight - 105;
	int menuPosY = (screenHeight / 2) - 55;
	int menuPosY2 = (screenHeight / 2) - 25;
	int menuPosY3 = (screenHeight / 2) + 20;
	int menuPosY4 = (screenHeight / 2) + 5;
	int menuPosY5 = (screenHeight / 2) + 35;
	int configurationsPosY = (screenHeight / 2) - 200;
	int configurationsPosY2 = (screenHeight / 2) - 120;
	int configurationsPosY3 = (screenHeight / 2) - 20;
	int configurationsPosY4 = (screenHeight / 2) + 60;
	int configurationsPosY5 = (screenHeight / 2) + 160;
	int configurationsPosY6 = (screenHeight / 2) + 240;
	int resolutionsPosY = (screenHeight / 2) - 280;
	int resolutionsPosY2 = (screenHeight / 2) - 285;
	int resolutionsPosY3 = (screenHeight / 2) - 130;
	int resolutionsPosY4 = (screenHeight / 2) - 40;
	int resolutionsPosY5 = (screenHeight / 2) + 80;
	int resolutionsPosY6 = (screenHeight / 2) + 170;
	int audioPosY = (screenHeight / 2) - 280;
	int audioPosY2 = (screenHeight / 2) - 285;
	int audioPosY3 = (screenHeight / 2) - 120;
	int audioPosY4 = (screenHeight / 2) - 30;
	int audioPosY5 = (screenHeight / 2) + 60;
	int audioPosY6 = (screenHeight / 2) + 150;
	int versionPosY = screenHeight - 50;

	int textsSize = 40;
	int textsSize2 = 100;
	int textsSize3 = 30;
	int textsSize4 = 20;
	int textsSize5 = 70;
	int textsSize6 = 80;
	int textsSize7 = 60;
	int keysSize = 50;

	void updateVariables()
	{
		backgroundPos = 0;

		recSizeLines = 5;
		recHeight = 75;

		leftDownButtonPosX = 40;
		centerDownButtonPosX = (screenWidth / 2) - 40;
		rightDownButtonsPosX = screenWidth - 120;
		downButtonsPosY = screenHeight - 120;

		arrowsPosX = (screenWidth / 2) - 470;
		arrowsPosY = (screenHeight / 2) - 210;

		leftArrowPosX = 80;
		rightArrowPosX = screenWidth - 170;
		leftAndRightArrowPosY = screenHeight / 2;

		paddlePosX = (screenWidth / 2) - 70;
		ballPosX = (screenWidth / 2) - 20;
		entitiesPosY = (screenHeight / 2) + 30;

		buttonsUpLeftPosX = 5;
		buttonsUpRightPosX = screenWidth - 85;
		buttonsUpPosY = 0;

		pauseAudioButtonPosX = (screenWidth / 2) - 40;
		pauseAudioButtonPosY = (screenHeight / 2) + 250;

		paddleControlsLeftArrowPosX = 100;
		paddleControlsRightArrowPosX = screenWidth - 190;
		paddleControlsDefaultPosX = (screenWidth / 2) - 150;
		paddleControlsRec1PosX = (screenWidth / 2) - 150;
		paddleControlsRec2PosX = (screenWidth / 2) + 50;
		paddleControlsKey1PosX = (screenWidth / 2) - 116;
		paddleControlsKey2PosX = (screenWidth / 2) + 82;
		paddleControlsRecPosY = (screenHeight / 2) - 10;
		paddleControlsKeysPosY = (screenHeight / 2) + 16;
		paddleControlsrecSize = 100;

		resultLeftButtons = (screenWidth / 2) - 380;
		resultRightButtons = (screenWidth / 2) + 90;
		resultCenterButton = (screenWidth / 2) - 150;
		resultUpButtons = (screenHeight / 2) + 80;
		resultDownButtons = (screenHeight / 2) + 230;

		instructions2Block1PosX = (screenWidth / 2) - 330;
		instructions2Block2PosX = (screenWidth / 2) - 65;
		instructions2Block3PosX = (screenWidth / 2) + 205;
		instructions2Block1PosX2 = (screenWidth / 2) - 450;
		instructions2Block3PosX2 = (screenWidth / 2) + 325;
		instructions2Block1PosY = 320;
		instructions2Block2PosY = 390;
		instructions2Block3PosY = 460;
		instructions2Rec1PosX = (screenWidth / 2) - 200;
		instructions2Rec2PosX = (screenWidth / 2) + 130;
		instructions2Rec1PosX2 = (screenWidth / 2) - 220;
		instructions2Rec2PosX2 = (screenWidth / 2) + 150;
		instructions2RecPosY = (screenHeight / 2) + 150;
		instructions2RecPosX = (screenWidth / 2) - 70;
		instructions2Rec1PosY = 550;
		instructions2Rec2PosY = 630;

		instructions3PowerupPosX = (screenWidth / 2) - 30;
		instructions3PowerupPosY = (screenHeight / 2) - 240;
		instructions3PowerupPosY2 = (screenHeight / 2) + 80;
		instructions4PowerupPosY3 = (screenHeight / 2) + 40;

		configurationsRecPosX = (screenWidth / 2) - 270;
		configurationsRecPosX2 = (screenWidth / 2) - 80;
		configurationsRecPosX3 = (screenWidth / 2) + 140;
		configurationsRecPosX4 = (screenWidth / 2) - 198;
		configurationsRecPosX5 = (screenWidth / 2) - 28;
		configurationsRecPosX6 = (screenWidth / 2) + 138;
		configurationsRecPosX7 = (screenWidth / 2) - 120;
		configurationsRecPosX8 = (screenWidth / 2) + 20;
		configurationsRecPosY = (screenHeight / 2) - 140;
		configurationsRecPosY2 = (screenHeight / 2) + 40;
		configurationsRecPosY3 = (screenHeight / 2) + 220;
		configurationsRecWidth = 140;
		configurationsRecWidth2 = 170;
		configurationsRecWidth3 = 60;
		configurationsRecWidth4 = 100;

		resolutionsRecPosX = (screenWidth / 2) - 190;
		resolutionsRecPosX2 = (screenWidth / 2) - 50;
		resolutionsRecPosX3 = (screenWidth / 2) + 90;
		resolutionsRecPosX4 = (screenWidth / 2) - 190;
		resolutionsRecPosX5 = (screenWidth / 2) - 50;
		resolutionsRecPosX6 = (screenWidth / 2) + 90;
		resolutionsRecPosY = (screenHeight / 2) - 60;
		resolutionsRecPosY2 = (screenHeight / 2) + 150;
		resolutionsRecWidth = 110;

		audioRecPosX = (screenWidth / 2) - 250;
		audioRecPosX2 = (screenWidth / 2) - 65;
		audioRecPosX3 = (screenWidth / 2) + 120;
		audioRecPosX4 = (screenWidth / 2) - 250;
		audioRecPosX5 = (screenWidth / 2) - 65;
		audioRecPosX6 = (screenWidth / 2) + 120;
		audioRecPosY = (screenHeight / 2) - 50;
		audioRecPosY2 = (screenHeight / 2) + 130;
		audioRecWidth = 135;

		creditsRec1PosX = 50;
		creditsRec2PosX = (screenWidth / 2) + 185;
		creditsRecPosY = (screenHeight / 2) - 280;
		creditsRecSize = (screenWidth / 2) - 240;

		menuRecWidth = (screenWidth / 2) - 350;
		menuRec2PosX = (screenWidth / 2) + 290;
		menuTitlePosX = (screenWidth / 2) - 250;
		menuTitlePosY = (screenHeight / 2) - 380;
		menuHighScorePosX = screenWidth - 270;
		menuHighScorePosY = (screenHeight / 2) - 100;

		settingRecWidth = (screenWidth / 2) - 380;
		settingRec2PosX = (screenWidth / 2) + 330;

		rec1PosX = 50;
		recPosY = (screenHeight / 2) - 280;
		buttonsPosX = (screenWidth / 2) - 150;
		buttonPosY = (screenHeight / 2) - 180;
		button2PosY = (screenHeight / 2) - 70;
		button3PosY = (screenHeight / 2) + 40;
		button4PosY = (screenHeight / 2) + 150;
		button5PosY = (screenHeight / 2) + 260;

		// Texts
		actualScorePosX = 100;
		actualScorePosX2 = 170;
		livesPosX = (screenWidth / 2) - 50;
		livesPosX2 = (screenWidth / 2) - 140;
		livesPosX3 = (screenWidth / 2) - 100;
		livesPosX4 = (screenWidth / 2) - 60;
		livesPosX5 = (screenWidth / 2) - 20;
		livesPosX6 = (screenWidth / 2) + 20;
		livesPosX7 = (screenWidth / 2) + 60;
		livesPosX8 = (screenWidth / 2) + 100;
		highScorePosX = screenWidth - 330;
		highScorePosX2 = screenWidth - 290;
		pausePosX = (screenWidth / 2) - 170;
		enterPosX = (screenWidth / 2) - 240;
		spacebarPosX = (screenWidth / 2) - 290;
		paddleControlsText1PosX = (screenWidth / 2) - 370;
		paddleControlsText2PosX = (screenWidth / 2) - 375;
		paddleControlsText3PosX = (screenWidth / 2) - 260;
		paddleControlsText4PosX = (screenWidth / 2) - 265;
		paddleControlsText5PosX = (screenWidth / 2) - 365;
		paddleControlsText6PosX = (screenWidth / 2) - 270;
		settingsTextPosX = (screenWidth / 2) - 290;
		settingsTextPosX2 = (screenWidth / 2) - 295;
		paddleColorTextPosX = (screenWidth / 2) - 530;
		paddleColorTextPosX2 = (screenWidth / 2) - 535;
		paddleColorTextPosX3 = (screenWidth / 2) - 330;
		paddleColorTextPosX4 = (screenWidth / 2) - 335;
		paddleColorTextPosX5 = (screenWidth / 2) - 185;
		paddleColorTextPosX6 = (screenWidth / 2) - 190;
		ballColorTextPosX = (screenWidth / 2) - 480;
		ballColorTextPosX2 = (screenWidth / 2) - 485;
		ballColorTextPosX3 = (screenWidth / 2) - 330;
		ballColorTextPosX4 = (screenWidth / 2) - 335;
		ballColorTextPosX5 = (screenWidth / 2) - 130;
		ballColorTextPosX6 = (screenWidth / 2) - 135;
		configurationsTextPosX = (screenWidth / 2) - 260;
		configurationsTextPosX2 = (screenWidth / 2) - 265;
		gameOverPosX = (screenWidth / 2) - 250;
		resultPosX = (screenWidth / 2) - 200;
		creditsPosX = (screenWidth / 2) - 125;
		creditsPosX2 = (screenWidth / 2) - 130;
		creditsPosX3 = (screenWidth / 2) - 115;
		creditsPosX4 = (screenWidth / 2) - 150;
		creditsPosX5 = (screenWidth / 2) - 100;
		creditsPosX6 = (screenWidth / 2) - 150;
		creditsPosX7 = (screenWidth / 2) - 205;
		creditsPosX8 = (screenWidth / 2) - 245;
		creditsPosX9 = (screenWidth / 2) - 80;
		creditsPosX10 = (screenWidth / 2) - 75;
		creditsPosX11 = (screenWidth / 2) - 80;
		credits2PosX = (screenWidth / 2) - 50;
		credits2PosX2 = (screenWidth / 2) - 215;
		credits2PosX3 = (screenWidth / 2) - 275;
		credits2PosX4 = (screenWidth / 2) - 220;
		credits2PosX5 = (screenWidth / 2) - 225;
		credits2PosX6 = (screenWidth / 2) - 230;
		credits2PosX7 = (screenWidth / 2) - 280;
		instructionsPosX = (screenWidth / 2) - 390;
		instructionsPosX2 = (screenWidth / 2) - 395;
		instructionsPosX3 = (screenWidth / 2) - 520;
		instructionsPosX4 = (screenWidth / 2) - 480;
		instructionsPosX5 = (screenWidth / 2) - 470;
		instructionsPosX6 = (screenWidth / 2) - 350;
		instructionsPosX7 = (screenWidth / 2) - 450;
		instructionsPosX8 = (screenWidth / 2) - 255;
		instructionsPosX9 = (screenWidth / 2) - 450;
		instructionsPosX10 = (screenWidth / 2) - 255;
		instructionsPosX11 = (screenWidth / 2) - 335;
		instructionsPosX12 = (screenWidth / 2) - 340;
		instructionsPosX13 = (screenWidth / 2) - 390;
		instructionsPosX14 = (screenWidth / 2) - 360;
		instructionsPosX15 = (screenWidth / 2) - 350;
		instructionsPosX16 = (screenWidth / 2) - 260;
		instructionsPosX17 = (screenWidth / 2) - 340;
		instructionsPosX18 = (screenWidth / 2) - 185;
		instructionsPosX19 = (screenWidth / 2) - 340;
		instructionsPosX20 = (screenWidth / 2) - 185;
		instructions2PosX = (screenWidth / 2) - 320;
		instructions2PosX2 = (screenWidth / 2) - 325;
		instructions2PosX3 = (screenWidth / 2) - 350;
		instructions2PosX4 = (screenWidth / 2) - 360;
		instructions2PosX5 = (screenWidth / 2) - 180;
		instructions2PosX6 = (screenWidth / 2) + 150;
		instructions2PosX7 = (screenWidth / 2) - 500;
		instructions2PosX8 = (screenWidth / 2) - 460;
		instructions2PosX9 = (screenWidth / 2) - 200;
		instructions2PosX10 = (screenWidth / 2) + 170;
		instructions3PosX = (screenWidth / 2) - 165;
		instructions3PosX2 = (screenWidth / 2) - 170;
		instructions3PosX3 = (screenWidth / 2) - 100;
		instructions3PosX4 = (screenWidth / 2) - 330;
		instructions3PosX5 = (screenWidth / 2) - 340;
		instructions3PosX6 = (screenWidth / 2) - 335;
		instructions3PosX7 = (screenWidth / 2) - 90;
		instructions3PosX8 = (screenWidth / 2) - 300;
		instructions3PosX9 = (screenWidth / 2) - 180;
		instructions3PosX10 = (screenWidth / 2) - 170;
		instructions3PosX11 = (screenWidth / 2) - 510;
		instructions3PosX12 = (screenWidth / 2) - 520;
		instructions3PosX13 = (screenWidth / 2) - 515;
		instructions3PosX14 = (screenWidth / 2) - 160;
		instructions3PosX15 = (screenWidth / 2) - 480;
		instructions3PosX16 = (screenWidth / 2) - 280;
		instructions4PosX = (screenWidth / 2) - 90;
		instructions4PosX2 = (screenWidth / 2) - 330;
		instructions4PosX3 = (screenWidth / 2) - 140;
		instructions4PosX4 = (screenWidth / 2) - 80;
		instructions4PosX5 = (screenWidth / 2) - 500;
		instructions4PosX6 = (screenWidth / 2) - 220;
		instructions4PosX7 = (screenWidth / 2) - 120;
		instructions4PosX8 = (screenWidth / 2) - 115;
		menuPosX = screenWidth - 200;
		menuPosX2 = screenWidth - 217;
		menuPosX3 = screenWidth - 225;
		menuPosX4 = screenWidth - 235;
		menuPosX5 = screenWidth - 217;
		menuPosX6 = screenWidth - 250;
		menuPosX7 = screenWidth - 190;
		configurationsPosX = (screenWidth / 2) - 220;
		configurationsPosX2 = (screenWidth / 2) - 250;
		configurationsPosX3 = (screenWidth / 2) - 60;
		configurationsPosX4 = (screenWidth / 2) + 160;
		configurationsPosX5 = (screenWidth / 2) - 50;
		configurationsPosX6 = (screenWidth / 2) - 178;
		configurationsPosX7 = (screenWidth / 2) - 8;
		configurationsPosX8 = (screenWidth / 2) + 158;
		configurationsPosX9 = (screenWidth / 2) - 100;
		configurationsPosX10 = (screenWidth / 2) - 105;
		configurationsPosX11 = (screenWidth / 2) + 45;
		resolutionsPosX = (screenWidth / 2) - 180;
		resolutionsPosX2 = (screenWidth / 2) - 185;
		resolutionsPosX3 = (screenWidth / 2) - 50;
		resolutionsPosX4 = (screenWidth / 2) - 170;
		resolutionsPosX5 = (screenWidth / 2) - 35;
		resolutionsPosX6 = (screenWidth / 2) + 105;
		resolutionsPosX7 = (screenWidth / 2) - 60;
	    resolutionsPosX8 = (screenWidth / 2) - 170;
		resolutionsPosX9 = (screenWidth / 2) - 30;
		resolutionsPosX10 = (screenWidth / 2) + 105;
		audioPosX = (screenWidth / 2) - 90;
		audioPosX2 = (screenWidth / 2) - 95;
		audioPosX3 = (screenWidth / 2) - 55;
		audioPosX4 = (screenWidth / 2) - 230;
		audioPosX5 = (screenWidth / 2) - 30;
		audioPosX6 = (screenWidth / 2) + 145;
		audioPosX7 = (screenWidth / 2) - 70;
		audioPosX8 = (screenWidth / 2) - 230;
		audioPosX9 = (screenWidth / 2) - 30;
		audioPosX10 = (screenWidth / 2) + 145;
		versionPosX = 20;
		versionPosX2 = screenWidth - 250;

		textsPosY = 0;
		textsPosY2 = 40;
		textsPosY3 = (screenHeight / 2) + 150;
		textsPosY4 = (screenHeight / 2) + 200;
		paddleControlsText1PosY = 80;
		paddleControlsText2PosY = 85;
		paddleControlsText3PosY = 170;
		paddleControlsText4PosY = 175;
		settingsTextPosY = (screenHeight / 2) - 310;
		settingsTextPosY2 = (screenHeight / 2) - 315;
		colorTextPosY = 90;
		colorTextPosY2 = 95;
		colorTextPosY3 = 80;
		colorTextPosY4 = 85;
		colorTextPosY5 = 170;
		colorTextPosY6 = 175;
		gameOverPosY = (screenHeight / 2) - 210;
		resultPosY = (screenHeight / 2) - 70;
		creditsPosY = (screenHeight / 2) - 310;
		creditsPosY2 = (screenHeight / 2) - 315;
		creditsPosY3 = (screenHeight / 2) - 170;
		creditsPosY4 = (screenHeight / 2) - 120;
		creditsPosY5 = (screenHeight / 2) - 40;
		creditsPosY6 = (screenHeight / 2) + 10;
		creditsPosY7 = (screenHeight / 2) + 60;
		creditsPosY8 = (screenHeight / 2) + 110;
		creditsPosY9 = (screenHeight / 2) + 160;
		creditsPosY10 = (screenHeight / 2) + 210;
		creditsPosY11 = (screenHeight / 2) + 260;
		credits2PosY = (screenHeight / 2) - 120;
		credits2PosY2 = (screenHeight / 2) - 70;
		credits2PosY3 = (screenHeight / 2) - 20;
		credits2PosY4 = (screenHeight / 2) + 30;
		credits2PosY5 = (screenHeight / 2) + 80;
		credits2PosY6 = (screenHeight / 2) + 130;
		credits2PosY7 = (screenHeight / 2) + 180;
		instructionsPosY = 70;
		instructionsPosY2 = 75;
		instructionsPosY3 = 220;
		instructionsPosY4 = 270;
		instructionsPosY5 = 370;
		instructionsPosY6 = 420;
		instructionsPosY7 = 520;
		instructionsPosY8 = 570;
		instructionsPosY9 = 670;
		instructionsPosY10 = 720;
		instructions2PosY = (screenHeight / 2) + 166;
		instructions2PosY2 = (screenHeight / 2) + 200;
		instructions2PosY3 = (screenHeight / 2) + 166;
		instructions3PosY = (screenHeight / 2) - 190;
		instructions3PosY2 = (screenHeight / 2) - 140;
		instructions3PosY3 = (screenHeight / 2) - 90;
		instructions3PosY4 = (screenHeight / 2) - 40;
		instructions3PosY5 = (screenHeight / 2) + 140;
		instructions3PosY6 = (screenHeight / 2) + 180;
		instructions3PosY7 = (screenHeight / 2) + 230;
		instructions4PosY = (screenHeight / 2) - 190;
		instructions4PosY2 = (screenHeight / 2) - 140;
		instructions4PosY3 = (screenHeight / 2) - 90;
		instructions4PosY4 = (screenHeight / 2) + 90;
		instructions4PosY5 = (screenHeight / 2) + 140;
		instructions4PosY6 = (screenHeight / 2) + 190;
		instructions4PosY7 = screenHeight - 100;
		instructions4PosY8 = screenHeight - 105;
		menuPosY = (screenHeight / 2) - 55;
		menuPosY2 = (screenHeight / 2) - 25;
		menuPosY3 = (screenHeight / 2) + 20;
		menuPosY4 = (screenHeight / 2) + 5;
		menuPosY5 = (screenHeight / 2) + 35;
		configurationsPosY = (screenHeight / 2) - 200;
		configurationsPosY2 = (screenHeight / 2) - 120;
		configurationsPosY3 = (screenHeight / 2) - 20;
		configurationsPosY4 = (screenHeight / 2) + 60;
		configurationsPosY5 = (screenHeight / 2) + 160;
		configurationsPosY6 = (screenHeight / 2) + 240;
		resolutionsPosY = (screenHeight / 2) - 280;
		resolutionsPosY2 = (screenHeight / 2) - 285;
		resolutionsPosY3 = (screenHeight / 2) - 130;
		resolutionsPosY4 = (screenHeight / 2) - 40;
		resolutionsPosY5 = (screenHeight / 2) + 80;
		resolutionsPosY6 = (screenHeight / 2) + 170;
		audioPosY = (screenHeight / 2) - 280;
		audioPosY2 = (screenHeight / 2) - 285;
		audioPosY3 = (screenHeight / 2) - 120;
		audioPosY4 = (screenHeight / 2) - 30;
		audioPosY5 = (screenHeight / 2) + 60;
		audioPosY6 = (screenHeight / 2) + 150;
		versionPosY = screenHeight - 50;

		textsSize = 40;
		textsSize2 = 100;
		textsSize3 = 30;
		textsSize4 = 20;
		textsSize5 = 70;
		textsSize6 = 80;
		textsSize7 = 60;
		keysSize = 50;
	}
}