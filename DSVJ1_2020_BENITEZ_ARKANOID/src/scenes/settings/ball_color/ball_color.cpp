#include "ball_color.h"

using namespace pong;
using namespace gameplay;
using namespace balls;

namespace pong
{
	namespace ball_color
	{
		Color ballColor = WHITE;
		static int numberColorBall = 0;

		static Color leftArrowButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color settingsButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		void init()
		{
			ballColor = WHITE;

			textures::ballTexture = LoadTexture("res/assets/textures/ball.png");
			textures::ballTexture.width = ball.radius * 2;
			textures::ballTexture.height = ball.radius * 2;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftArrowPosX, pos::leftAndRightArrowPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
			{
				leftArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					numberColorBall--;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightArrowPosX, pos::leftAndRightArrowPosY, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					numberColorBall++;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX, pos::downButtonsPosY, (float)textures::settingsCircleButton.width, (float)textures::settingsCircleButton.height }))
			{
				settingsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::SETTINGS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::BALLCOLOR;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::BALLCOLOR;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				leftArrowButtonStatus = WHITE;
				rightArrowButtonStatus = WHITE;
				menuButtonStatus = WHITE;
				settingsButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			if (numberColorBall < 0) numberColorBall = 8;
			else if (numberColorBall > 8) numberColorBall = 0;

			if (numberColorBall == 0) ballColor = WHITE;
			else if (numberColorBall == 1) ballColor = BLUE;
			else if (numberColorBall == 2) ballColor = VIOLET;
			else if (numberColorBall == 3) ballColor = GREEN;
			else if (numberColorBall == 4) ballColor = RED;
			else if (numberColorBall == 5) ballColor = PINK;
			else if (numberColorBall == 6) ballColor = ORANGE;
			else if (numberColorBall == 7) ballColor = YELLOW;
			else if (numberColorBall == 8) ballColor = DARKGRAY;

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawTexture(textures::leftArrow, (int)pos::leftArrowPosX, (int)pos::leftAndRightArrowPosY, leftArrowButtonStatus);
			DrawTexture(textures::rightArrow, (int)pos::rightArrowPosX, (int)pos::leftAndRightArrowPosY, rightArrowButtonStatus);
			DrawTexture(textures::menuCircleButton, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::settingsCircleButton, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, settingsButtonStatus);

			if (screenWidth >= 1200)
			{
				DrawText("Select a color for the ball", pos::ballColorTextPosX, pos::colorTextPosY, pos::textsSize5, BLACK);
				DrawText("Select a color for the ball", pos::ballColorTextPosX2, pos::colorTextPosY2, pos::textsSize5, WHITE);
			}
			else
			{
				DrawText("Select a color", pos::ballColorTextPosX3, pos::colorTextPosY3, pos::textsSize5, BLACK);
				DrawText("Select a color", pos::ballColorTextPosX4, pos::colorTextPosY4, pos::textsSize5, WHITE);
				DrawText("for the ball", pos::ballColorTextPosX5, pos::colorTextPosY5, pos::textsSize5, BLACK);
				DrawText("for the ball", pos::ballColorTextPosX6, pos::colorTextPosY6, pos::textsSize5, WHITE);
			}

			DrawTexture(textures::arrows, (int)pos::arrowsPosX, (int)pos::arrowsPosY, WHITE);
			DrawTexture(textures::ballTexture, (int)pos::ballPosX, (int)pos::entitiesPosY, ballColor);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::ballTexture);
		}
	}
}