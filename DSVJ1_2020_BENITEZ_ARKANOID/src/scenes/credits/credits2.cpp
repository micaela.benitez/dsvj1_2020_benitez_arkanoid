#include "credits2.h"

using namespace pong;
using namespace gameplay;

namespace pong
{
	namespace credits2
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color resolutionsButtonStatus = WHITE;
		static Color audioButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::leftDownButtonPosX, pos::downButtonsPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
			{
				leftArrowButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::CREDITS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::rightDownButtonsPosX,  pos::downButtonsPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::resolution.width, (float)textures::resolution.height }))
			{
				resolutionsButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::CREDITS2;
					pong::game::currentScene = pong::game::Scene::RESOLUTIONS;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
			{
				audioButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::previousScene = pong::game::Scene::CREDITS2;
					pong::game::currentScene = pong::game::Scene::AUDIO;
				}
			}
			else
			{
				leftArrowButtonStatus = WHITE;
				menuButtonStatus = WHITE;
				resolutionsButtonStatus = WHITE;
				audioButtonStatus = WHITE;
			}

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundMenu, (int)pos::backgroundPos, (int)pos::backgroundPos, WHITE);
			DrawText("Credits", pos::creditsPosX, pos::creditsPosY, pos::textsSize5, BLACK);
			DrawText("Credits", pos::creditsPosX2, pos::creditsPosY2, pos::textsSize5, WHITE);
			DrawRectangle((int)pos::creditsRec1PosX, (int)pos::creditsRecPosY, (int)pos::creditsRecSize, (int)pos::recSizeLines, BLACK);
			DrawRectangle((int)pos::creditsRec2PosX, (int)pos::creditsRecPosY, (int)pos::creditsRecSize, (int)pos::recSizeLines, BLACK);
			DrawText("Arkanoid V 2.0", pos::versionPosX2, pos::versionPosY, pos::textsSize3, WHITE);

			DrawText("Audio", pos::credits2PosX, pos::credits2PosY, pos::textsSize, WHITE);
			DrawText("Mobeyee from freesound.org", pos::credits2PosX2, pos::credits2PosY2, pos::textsSize3, BLACK);
			DrawText("Autistic Lucario from freesound.org", pos::credits2PosX3, pos::credits2PosY3, pos::textsSize3, BLACK);
			DrawText("Altemark from freesound.org", pos::credits2PosX4, pos::credits2PosY4, pos::textsSize3, BLACK);
			DrawText("Breviceps from freesound.org", pos::credits2PosX5, pos::credits2PosY5, pos::textsSize3, BLACK);
			DrawText("Michorvath from freesound.org", pos::credits2PosX6, pos::credits2PosY6, pos::textsSize3, BLACK);
			DrawText("Alexander from orangefreesounds.com", pos::credits2PosX7, pos::credits2PosY7, pos::textsSize3, BLACK);


			DrawTexture(textures::leftArrow, (int)pos::leftDownButtonPosX, (int)pos::downButtonsPosY, leftArrowButtonStatus);
			DrawTexture(textures::menuCircleButton, (int)pos::rightDownButtonsPosX, (int)pos::downButtonsPosY, menuButtonStatus);
			DrawTexture(textures::resolution, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, resolutionsButtonStatus);
			DrawTexture(textures::gameAudio, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, audioButtonStatus);
		}

		void deinit()
		{
		}
	}
}