#include "entities_motion.h"

using namespace pong;
using namespace gameplay;

void paddleMotion()
{	
	if (IsKeyDown(paddle_controls::key.leftPaddle)) player.x = (player.x - paddle.speed * GetFrameTime() > borderSize) ? player.x -= paddle.speed * GetFrameTime() : player.x = (float)borderSize;
    else if (IsKeyDown(paddle_controls::key.rightPaddle)) player.x = (player.x + paddle.speed * GetFrameTime() < (screenWidth - player.width - borderSize)) ? player.x += paddle.speed * GetFrameTime() : player.x = (screenWidth - player.width - borderSize);
}

void ballMotion()
{
    using namespace balls;

    static const int lostPoints = 10;
    static const int minimumPoints = 0;
    
    if (!ball.loss)
    {
        ball.position.x += ball.speed.x;
        ball.position.y += ball.speed.y;

        if (ball.position.x >= (screenWidth - ball.radius - borderSize) || ball.position.x <= ball.radius + borderSize)   // If the ball collides one side
        {
            ball.speed.x *= -1.0f;
            sounds.ballCollisionWalls = true;
            if (ball.speed.x > 0) ball.position.x += 10;
            else ball.position.x -= 10;
        }
        else if (ball.position.y <= (ball.radius + borderSize + informationBorderSize))   // If the ball hits the upper wall
        {
            ball.speed.y *= -1.0f;
            sounds.ballCollisionWalls = true;
            ball.position.y += 10;
        }
        else if (ball.position.y >= (screenHeight - ball.radius))   // If the ball is lost
        {
            ball.loss = true;       
            powerup.paddle = false;
            powerup.ball = false;
            playerLives--;
            actualScore -= lostPoints;
            if (actualScore < minimumPoints) actualScore = minimumPoints;
            sounds.death = true;
        }
        else if (CheckCollisionCircleRec(ball.position, (float)ball.radius, player))   // If the ball hits the paddle
        {
            ball.speed.y *= -1.0f;
            ball.speed.x = (ball.position.x - (player.x + (player.width / 2))) / (player.width / 2) * (speedXBallModifier * GetFrameTime());
            sounds.ballCollisionEntities = true;
            ball.position.y -= 10;
        }
    }    
}

void ballMotion(Ball& ball)
{
    ball.position.x += ball.speed.x;
    ball.position.y += ball.speed.y;

    if (ball.position.x >= (screenWidth - ball.radius - borderSize) || ball.position.x <= ball.radius + borderSize)   // If the ball collides with the side
    {
        ball.speed.x *= -1.0f;
        sounds.ballCollisionWalls = true;
        if (ball.speed.x > 0) ball.position.x += 10;
        else ball.position.x -= 10;
    }
    else if (ball.position.y <= (ball.radius + borderSize + informationBorderSize))   // If the ball hits the upper wall
    {
        ball.speed.y *= -1.0f;
        sounds.ballCollisionWalls = true;
        ball.position.y += 10;
    }
    else if (ball.position.y >= (screenHeight - ball.radius))   // If the ball is lost
    {
        ball.loss = true;
        powerup.paddle = false;
        powerup.ball = false;
        sounds.death = true;
    }
    else if (CheckCollisionCircleRec(ball.position, (float)ball.radius, player))   // If the ball hits the paddle
    {
        ball.speed.y *= -1.0f;
        ball.speed.x = (ball.position.x - (player.x + (player.width / 2))) / (player.width / 2) * (speedXBallModifier * GetFrameTime());
        sounds.ballCollisionEntities = true;
        ball.position.y -= 10;
    }
}

void ballLoss()
{
    using namespace balls;

    static const float initialBallSpeedX = 0.0f;

    if (ball.loss && !powerup.multiball1 && !powerup.multiball2)
    {
        ball.position = { player.x + player.width / 2, player.y - player.height / 2 };
        ball.speed = { initialBallSpeedX * GetFrameTime(), speedYBallModifier * GetFrameTime() };

        if (IsKeyDown(KEY_ENTER)) ball.loss = false;
    }
    else if (ball.loss && ball1.loss && ball2.loss)
    {
        ball.position = { player.x + player.width / 2, player.y - player.height / 2 };
        ball.speed = { initialBallSpeedX * GetFrameTime(), speedYBallModifier * GetFrameTime() };

        if (IsKeyDown(KEY_ENTER)) ball.loss = false;
    }
}

void ballVsBlocks(Ball& ball)
{
    static const int probabilityPowerup = 5;
    static const int probabilityLongPaddle = 2;
    static const int quantityPowerups = 4;

    static const int strongBlocksPoints = 20;
    static const int weakBlocksPoints = 15;

    srand((unsigned int)time(NULL));

    for (int i = 0; i < blocksPerLine; i++)
    {
        for (int j = 0; j < blocksPerColumn; j++)
        {
            if (block[i][j].active)
            {
                if (CheckCollisionCircleRec(ball.position, (float)ball.radius, { block[i][j].position.x - (block[i][j].size.x / 2), block[i][j].position.y - (block[i][j].size.y / 2), block[i][j].size.x, block[i][j].size.y }))
                {
                    if (!block[i][j].unbreakable && block[i][j].hits == 2)
                    {
                        block[i][j].active = false;
                        if (pong::configurations::powerupsConfigurationActive)
                        {
                            block[i][j].disabledPowerup = rand() % probabilityPowerup;
                            if (!block[i][j].disabledPowerup)
                            {
                                block[i][j].kindOfPowerup = (Powerups)(rand() % quantityPowerups);
                                block[i][j].positionPowerup = { block[i][j].position.x - block[i][j].size.x / 4, block[i][j].position.y + block[i][j].size.y / 2 };
                                if (block[i][j].kindOfPowerup == Powerups::PADDLE) powerup.longPaddle = rand() % probabilityLongPaddle;
                            }
                        }
                    }

                    if (!block[i][j].unbreakable)
                    {
                        if (block[i][j].strong) actualScore += strongBlocksPoints;
                        else actualScore += weakBlocksPoints;
                        if (actualScore > highScore) highScore = actualScore;
                        sounds.ballCollisionEntities = true;
                    }
                    else sounds.ballCollisionWalls = true;

                    block[i][j].hits++;

                    if (!powerup.fireball || block[i][j].unbreakable)
                    {
                        // Up
                        if (((ball.position.y + ball.radius) >= (block[i][j].position.y - block[i][j].size.y / 2)) &&
                            ((ball.position.y + ball.radius) < (block[i][j].position.y - block[i][j].size.y / 2 + ball.speed.y)) &&
                            ((fabs(ball.position.x - block[i][j].position.x)) < (block[i][j].size.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y > 0))
                        {
                            ball.speed.y *= -1;
                            ball.position.y -= 10;
                        }
                        // Down
                        else if (((ball.position.y - ball.radius) <= (block[i][j].position.y + block[i][j].size.y / 2)) &&
                            ((ball.position.y - ball.radius) > (block[i][j].position.y + block[i][j].size.y / 2 + ball.speed.y)) &&
                            ((fabs(ball.position.x - block[i][j].position.x)) < (block[i][j].size.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y < 0))
                        {
                            ball.speed.y *= -1;
                            ball.position.y += 10;
                        }
                        // Left
                        else if (((ball.position.x + ball.radius) >= (block[i][j].position.x - block[i][j].size.x / 2)) &&
                            ((ball.position.x + ball.radius) < (block[i][j].position.x - block[i][j].size.x / 2 + ball.speed.x)) &&
                            ((fabs(ball.position.y - block[i][j].position.y)) < (block[i][j].size.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x > 0))
                        {
                            ball.speed.x *= -1;
                            ball.position.x -= 10;
                        }
                        // Right
                        else if (((ball.position.x - ball.radius) <= (block[i][j].position.x + block[i][j].size.x / 2)) &&
                            ((ball.position.x - ball.radius) > (block[i][j].position.x + block[i][j].size.x / 2 + ball.speed.x)) &&
                            ((fabs(ball.position.y - block[i][j].position.y)) < (block[i][j].size.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x < 0))
                        {
                            ball.speed.x *= -1;
                            ball.position.x += 10;
                        }
                    }
                }
            }
        }
    }
}