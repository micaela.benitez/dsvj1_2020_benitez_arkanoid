#include "gameplay.h"

namespace pong
{
	namespace balls
	{
		Ball ball;
		Ball ball1;
		Ball ball2;
	}

	namespace gameplay
	{		
		using namespace balls;

		int screenWidth = 1200;
		int screenHeight = 900;

		int playerTotalLives = 5;
		int playerLives = playerTotalLives;

		Paddle paddle;
		
		Block block[blocksPerLine][blocksPerColumn];

		Rectangle player;
		int playerWidthLong = 220;
		int playerWidthShort = 100;
		
		int borderSize = 40;
		int informationBorderSize = 80;

		int actualScore = 0;
		int highScore = 0;
		int scorePerLevel = 0;
		bool playerWon = false;

		Vector2 mousePoint = { 0.0f, 0.0f };

		float speedXBallModifier = 400.0f;
		float speedYBallModifier = -400.0f;
		float speedPaddleModifier = 350.0f;

		int timer = 550;

		Powerup powerup;

		Sounds sounds;

		int level = 1;

		static Color menuButtonStatus = WHITE;
		static Color exitButtonStatus = WHITE;
		Color audioButtonStatus = WHITE;

		void init()
		{
			// Initialize paddle
			paddle.size = { 150, 30 };
			paddle.position = { (screenWidth / 2) - (paddle.size.x / 2), screenHeight - (paddle.size.y * 2) };

			// Initialize player
			player = { paddle.position.x, paddle.position.y, paddle.size.x, paddle.size.y };

			// Initialize ball
			ball.radius = 15;
			ball1.radius = 15;
			ball2.radius = 15;
						
			// Initialize blocks
			for (int i = 0; i < blocksPerLine; i++)
			{
				for (int j = 0; j < blocksPerColumn; j++)
				{
					block[i][j].size = { sizes::blocksSizeX, sizes::blocksSizeY };
					block[i][j].position = { j * sizes::blocksSizeX + sizes::blocksSizeX / 2 + borderSize, i * sizes::blocksSizeY + sizes::blocksSizeY / 2 + borderSize + informationBorderSize };
					block[i][j].weakHits = 2;
				}
			}
			
			// Textures
			textures::backgroundGameplay = LoadTexture ("res/assets/textures/background.png");
			textures::backgroundGameplay.width = (int)screenWidth;
			textures::backgroundGameplay.height = (int)screenHeight;

			textures::backgroundBorder = LoadTexture("res/assets/textures/border.png");
			textures::backgroundBorder.width = (int)screenWidth;
			textures::backgroundBorder.height = (int)screenHeight;

			textures::blocksTexture = LoadTexture("res/assets/textures/block.png");
			textures::blocksTexture.width = (int)sizes::blocksSizeX;
			textures::blocksTexture.height = (int)sizes::blocksSizeY;

			textures::unbreakableBlocks = LoadTexture("res/raw/textures/unbreakableblock.png");
			textures::unbreakableBlocks.width = (int)sizes::blocksSizeX;
			textures::unbreakableBlocks.height = (int)sizes::blocksSizeY;

			textures::brokenBlocks = LoadTexture("res/raw/textures/brokenblock.png");
			textures::brokenBlocks.width = (int)sizes::blocksSizeX;
			textures::brokenBlocks.height = (int)sizes::blocksSizeY;

			textures::brokenBlocks2 = LoadTexture("res/raw/textures/brokenblock2.png");
			textures::brokenBlocks2.width = (int)sizes::blocksSizeX;
			textures::brokenBlocks2.height = (int)sizes::blocksSizeY;

			textures::lives = LoadTexture("res/raw/textures/lives.png");
			textures::lives.width = sizes::livesWidth;
			textures::lives.height = sizes::livesHeight;

			textures::menuCircleButton = LoadTexture("res/raw/textures/menucircle.png");
			textures::menuCircleButton.width = sizes::sizeCircleButtons;
			textures::menuCircleButton.height = sizes::sizeCircleButtons;

			textures::exitCircleButton = LoadTexture("res/raw/textures/exitcircle.png");
			textures::exitCircleButton.width = sizes::sizeCircleButtons;
			textures::exitCircleButton.height = sizes::sizeCircleButtons;

			textures::rightArrow = LoadTexture("res/raw/textures/right.png");
			textures::rightArrow.width = sizes::sizeCircleButtons;
			textures::rightArrow.height = sizes::sizeCircleButtons;

			textures::leftArrow = LoadTexture("res/raw/textures/left.png");
			textures::leftArrow.width = sizes::sizeCircleButtons;
			textures::leftArrow.height = sizes::sizeCircleButtons;

			textures::multiballPowerup= LoadTexture("res/raw/textures/powerupmultiball.png");
			textures::fireballPowerup = LoadTexture("res/assets/textures/powerupfireball.png");
			textures::paddlePowerup = LoadTexture("res/assets/textures/poweruppaddle.png");
			textures::ballPowerup = LoadTexture("res/assets/textures/powerupball.png");

			// Audio
			audio::loadSounds();
			audio::loadMusic();
		}

		void initialize()
		{
			playerLives = playerTotalLives;
			scorePerLevel = actualScore;

			// Initialize paddle
			paddle.size = { 150, 30 };
			paddle.position = { (screenWidth / 2) - (paddle.size.x / 2), screenHeight - (paddle.size.y * 2) };
			paddle.speed = speedPaddleModifier;

			// Initialize player
			player = { paddle.position.x, paddle.position.y, paddle.size.x, paddle.size.y };

			// Initialize ball
			ball.active = true;
			ball.loss = true;
			ball1.active = true;
			ball1.loss = true;
			ball2.active = true;
			ball2.loss = true;			

			// Powerups
			powerup.multiball1 = false;
			powerup.multiball2 = false;
			powerup.fireball = false;
			powerup.paddle = false;
			powerup.longPaddle = false;
			powerup.ball = false;

			// Blocks
			for (int i = 0; i < blocksPerLine; i++)
			{
				for (int j = 0; j < blocksPerColumn; j++)
				{
					block[i][j].size = { sizes::blocksSizeX, sizes::blocksSizeY };
					block[i][j].position = { j * sizes::blocksSizeX + sizes::blocksSizeX / 2 + borderSize, i * sizes::blocksSizeY + sizes::blocksSizeY / 2 + borderSize + informationBorderSize };
					block[i][j].weakHits = 2;
					block[i][j].active = true;
					block[i][j].unbreakable = false;
					block[i][j].strong = false;
					block[i][j].hits = 0;
					block[i][j].disabledPowerup = true;
				}
			}
		}

		void update()
		{					
			checkIfThePlayerWon();
			audio::resultAudio();
			if (playerWon) sounds.winner = true;
			else if (playerLives == 0)
			{
				sounds.loser = true;
				if (pong::gameplay::playerLives <= 0) actualScore = scorePerLevel;
			}
			if (playerWon || playerLives == 0)
			{
				pong::game::currentScene = pong::game::Scene::RESULT;
				StopMusicStream(audio::gameMusic.gameplay);
			}
			
			if (IsKeyPressed(KEY_SPACE)) ball.active = (ball.active == false) ? true : false; // Pause

			if (playerLives != 0 && ball.active)
			{		
				paddleMotion();
				ballMotion();
				ballLoss();
				ballVsBlocks(ball);
				multiballPowerup();
				fireballPowerup();
				paddlePowerup();
				ballPowerup(ball);

				if (!ball1.loss)
				{
					ballMotion(ball1);
					ballVsBlocks(ball1);
					ballPowerup(ball1);
				} 
				if (!ball2.loss)
				{
					ballMotion(ball2);					
					ballVsBlocks(ball2);
					ballPowerup(ball2);
				}				
			}

			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpLeftPosX, pos::buttonsUpPosY, (float)textures::menuCircleButton.width, (float)textures::menuCircleButton.height }))
			{
				menuButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::currentScene = pong::game::Scene::MENU;		
					StopMusicStream(audio::gameMusic.gameplay);
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::buttonsUpRightPosX, pos::buttonsUpPosY, (float)textures::exitCircleButton.width, (float)textures::exitCircleButton.height }))
			{
				exitButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					pong::game::exitButton = true;					
				}
			}
			else
			{
				menuButtonStatus = WHITE;
				exitButtonStatus = WHITE;
			}					

			if (!ball.active && !ball.loss)   // Pause
			{
				if (CheckCollisionPointRec(mousePoint, { pos::pauseAudioButtonPosX, pos::pauseAudioButtonPosY, (float)textures::gameAudio.width, (float)textures::gameAudio.height }))
				{
					audioButtonStatus = GRAY;
					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
					{
						sounds.button = true;
						pong::game::previousScene = pong::game::Scene::GAMEPLAY;
						pong::game::currentScene = pong::game::Scene::AUDIO;
					}
				}
				else audioButtonStatus = WHITE;
			}

			PlayMusicStream(audio::gameMusic.gameplay);
			audio::gameplayAudio();
			sounds.ballCollisionEntities = false;
			sounds.ballCollisionWalls = false;
			sounds.death = false;
			sounds.button = false;
			sounds.powerup = false;
		}

		void draw()
		{
			DrawTexture(textures::backgroundGameplay, (int)pos::backgroundPos, informationBorderSize, WHITE);
			DrawTexture(textures::backgroundBorder, (int)pos::backgroundPos, informationBorderSize, WHITE);
			DrawTexture(textures::menuCircleButton, (int)pos::buttonsUpLeftPosX, (int)pos::buttonsUpPosY, menuButtonStatus);
			DrawTexture(textures::exitCircleButton, (int)pos::buttonsUpRightPosX, (int)pos::buttonsUpPosY, exitButtonStatus);
			
			if (playerLives != 0)
			{
				drawScreenDetails();
				levels();
				drawPaddle();
				drawPowerups();

				if (powerup.multiball1 || powerup.multiball2)
				{
					if (!ball.loss)
					{
						drawBall(ball);
						drawTimerFireballPowerup(ball);
					}
					if (!ball1.loss)
					{
						drawBall(ball1);
						drawTimerFireballPowerup(ball1);
					}
					if (!ball2.loss)
					{
						drawBall(ball2);
						drawTimerFireballPowerup(ball2);
					}
				}
				else
				{
					drawBall(ball);
					drawTimerFireballPowerup(ball);
				}

				drawPauseOptions();
			}
		}

		void deinit()
		{			
			UnloadTexture(textures::backgroundGameplay);
			UnloadTexture(textures::backgroundBorder);
			UnloadTexture(textures::blocksTexture);
			UnloadTexture(textures::unbreakableBlocks);
			UnloadTexture(textures::lives);
			UnloadTexture(textures::menuCircleButton);
			UnloadTexture(textures::exitCircleButton);
			UnloadTexture(textures::rightArrow);
			UnloadTexture(textures::leftArrow);
			UnloadTexture(textures::multiballPowerup);
			UnloadTexture(textures::fireballPowerup);
			UnloadTexture(textures::paddlePowerup);
			UnloadTexture(textures::ballPowerup);

			audio::unloadSounds();
			audio::unloadMusic();
		}
	}
}