#ifndef POWERUPS_H
#define POWERUPS_H

#include "scenes/gameplay/gameplay.h"

void multiballPowerup();
void fireballPowerup();
void paddlePowerup();
void ballPowerup(Ball& ball);

#endif