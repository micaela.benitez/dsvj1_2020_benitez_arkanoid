#ifndef BLOCK_H
#define BLOCK_H

#include "raylib.h"

enum class Powerups { MULTIBALL, FIREBALL, PADDLE, BALL };

struct Block
{
    Vector2 size;
    Vector2 position;;
    bool active;
    bool unbreakable;
    bool strong;
    int weakHits;
    int hits;

    // Current texture block
    Texture2D currentBlockTexture;

    // Block's powerups
    Powerups kindOfPowerup;
    bool disabledPowerup;
    Vector2 positionPowerup;
};

#endif