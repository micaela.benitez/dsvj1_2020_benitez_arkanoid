#include "drawings.h"

using namespace pong;
using namespace gameplay;

void drawPaddle()
{
    using namespace paddle_color;
    
    if (powerup.paddle && powerup.longPaddle)
    {
        if (paddleColor == PaddleColor::cBLUE) DrawTexture(textures::blueLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cVIOLET) DrawTexture(textures::violetLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cGREEN) DrawTexture(textures::greenLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cRED) DrawTexture(textures::redLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cPINK) DrawTexture(textures::pinkLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cORANGE) DrawTexture(textures::orangeLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cYELLOW) DrawTexture(textures::yellowLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cGRAY) DrawTexture(textures::grayLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cBLACK) DrawTexture(textures::blackLongPaddleTexture, (int)player.x, (int)player.y, WHITE);
        #if DEBUG 
        DrawRectangleLines(player.x, player.y, textures::blueLongPaddleTexture.width, textures::blueLongPaddleTexture.height, WHITE);
        #endif
    }
    else if (powerup.paddle && !powerup.longPaddle)
    {
        if (paddleColor == PaddleColor::cBLUE) DrawTexture(textures::blueShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cVIOLET) DrawTexture(textures::violetShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cGREEN) DrawTexture(textures::greenShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cRED) DrawTexture(textures::redShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cPINK) DrawTexture(textures::pinkShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cORANGE) DrawTexture(textures::orangeShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cYELLOW) DrawTexture(textures::yellowShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cGRAY) DrawTexture(textures::grayShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cBLACK) DrawTexture(textures::blackShortPaddleTexture, (int)player.x, (int)player.y, WHITE);
        #if DEBUG 
        DrawRectangleLines(player.x, player.y, textures::blueShortPaddleTexture.width, textures::blueShortPaddleTexture.height, WHITE);
        #endif
    }
    else
    {
        if (paddleColor == PaddleColor::cBLUE) DrawTexture(textures::bluePaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cVIOLET) DrawTexture(textures::violetPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cGREEN) DrawTexture(textures::greenPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cRED) DrawTexture(textures::redPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cPINK) DrawTexture(textures::pinkPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cORANGE) DrawTexture(textures::orangePaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cYELLOW) DrawTexture(textures::yellowPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cGRAY) DrawTexture(textures::grayPaddleTexture, (int)player.x, (int)player.y, WHITE);
        else if (paddleColor == PaddleColor::cBLACK) DrawTexture(textures::blackPaddleTexture, (int)player.x, (int)player.y, WHITE);
        #if DEBUG 
        DrawRectangleLines(player.x, player.y, textures::bluePaddleTexture.width, textures::bluePaddleTexture.height, WHITE);
        #endif
    }
}

void drawBall(Ball& ball)
{
    DrawTexture(textures::ballTexture, (int)(ball.position.x - ball.radius), (int)(ball.position.y - ball.radius), pong::ball_color::ballColor);
    #if DEBUG 
    DrawRectangleLines(ball.position.x - ball.radius, ball.position.y - ball.radius, textures::ballTexture.width, textures::ballTexture.height, WHITE);
    #endif
}

void drawScreenDetails()
{
    using namespace balls;
    
    DrawText("Actual score", pos::actualScorePosX, pos::textsPosY, pos::textsSize, RED);
    DrawText("Lives", pos::livesPosX, pos::textsPosY, pos::textsSize, RED);
    DrawText("High Score", pos::highScorePosX, pos::textsPosY, pos::textsSize, RED);

    DrawText(TextFormat("%06i", actualScore ), pos::actualScorePosX2, pos::textsPosY2, pos::textsSize, LIGHTGRAY);
    DrawText(TextFormat("%06i", highScore ), pos::highScorePosX2, pos::textsPosY2, pos::textsSize, LIGHTGRAY);

    if (playerTotalLives == 7)
    {
        if (playerLives >= 1) DrawTexture(textures::lives, pos::livesPosX2, pos::textsPosY2, WHITE);
        if (playerLives >= 2) DrawTexture(textures::lives, pos::livesPosX3, pos::textsPosY2, WHITE);
        if (playerLives >= 3) DrawTexture(textures::lives, pos::livesPosX4, pos::textsPosY2, WHITE);
        if (playerLives >= 4) DrawTexture(textures::lives, pos::livesPosX5, pos::textsPosY2, WHITE);
        if (playerLives >= 5) DrawTexture(textures::lives, pos::livesPosX6, pos::textsPosY2, WHITE);
        if (playerLives >= 6) DrawTexture(textures::lives, pos::livesPosX7, pos::textsPosY2, WHITE);
        if (playerLives >= 7) DrawTexture(textures::lives, pos::livesPosX8, pos::textsPosY2, WHITE);
    }
    else if (playerTotalLives == 5)
    {
        if (playerLives >= 1) DrawTexture(textures::lives, pos::livesPosX3, pos::textsPosY2, WHITE);
        if (playerLives >= 2) DrawTexture(textures::lives, pos::livesPosX4, pos::textsPosY2, WHITE);
        if (playerLives >= 3) DrawTexture(textures::lives, pos::livesPosX5, pos::textsPosY2, WHITE);
        if (playerLives >= 4) DrawTexture(textures::lives, pos::livesPosX6, pos::textsPosY2, WHITE);
        if (playerLives >= 5) DrawTexture(textures::lives, pos::livesPosX7, pos::textsPosY2, WHITE);
    }
    else
    {
        if (playerLives >= 1) DrawTexture(textures::lives, pos::livesPosX4, pos::textsPosY2, WHITE);
        if (playerLives >= 2) DrawTexture(textures::lives, pos::livesPosX5, pos::textsPosY2, WHITE);
        if (playerLives >= 3) DrawTexture(textures::lives, pos::livesPosX6, pos::textsPosY2, WHITE);
    }

    if (ball.loss)
    {
        if (playerLives == 5)
        {
            DrawText("Press ENTER to start the game", pos::enterPosX, pos::textsPosY3, pos::textsSize3, WHITE);
            DrawText("Press SPACEBAR to pause at any time", pos::spacebarPosX, pos::textsPosY4, pos::textsSize3, WHITE);
            ball.active = true;
        }
        else
        {
            if (ball.loss && !powerup.multiball1 && !powerup.multiball2)
            {
                DrawText("Press ENTER to throw the ball", pos::enterPosX, pos::textsPosY3, pos::textsSize3, WHITE);
                ball.active = true;
            }
        }
    }
}

void drawPauseOptions()
{
    using namespace balls;
    
    if (!ball.active && !ball.loss)   // You cannot pause the game if ball.active is false
    {
        DrawText("PAUSE", pos::pausePosX, pos::textsPosY3, pos::textsSize2, WHITE);
        DrawTexture(textures::gameAudio, pos::pauseAudioButtonPosX, pos::pauseAudioButtonPosY, audioButtonStatus);
    }
}

void drawPowerups()
{
    using namespace balls;

    static const float speedPowerups = 3.0f;
    static const int countdownTimer = 550;

    for (int i = 0; i < blocksPerLine; i++)
    {
        for (int j = 0; j < blocksPerColumn; j++)
        {
            if ((!block[i][j].disabledPowerup && ball1.loss && ball2.loss) || (!block[i][j].disabledPowerup && !ball1.loss && !ball2.loss && block[i][j].kindOfPowerup != Powerups::MULTIBALL))
            {
                block[i][j].positionPowerup.y += speedPowerups;

                if (block[i][j].kindOfPowerup == Powerups::MULTIBALL)
                {
                    if (CheckCollisionRecs(player, { block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, (float)textures::multiballPowerup.width, (float)textures::multiballPowerup.height }))
                    {
                        block[i][j].positionPowerup.y = (float)screenHeight;       
                        powerup.multiball1 = true;
                        powerup.multiball2 = true;
                        sounds.powerup = true;
                    }   
                    else if (ball.loss) block[i][j].positionPowerup.y = (float)screenHeight;
                    else
                    {
                        DrawTexture(textures::multiballPowerup, (int)(block[i][j].positionPowerup.x), (int)(block[i][j].positionPowerup.y), WHITE);
                        #if DEBUG 
                        DrawRectangleLines(block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, textures::multiballPowerup.width, textures::multiballPowerup.height, WHITE);
                        #endif
                    }
                }
                else if (block[i][j].kindOfPowerup == Powerups::FIREBALL)
                {
                    if (CheckCollisionRecs(player, { block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, (float)textures::fireballPowerup.width, (float)textures::fireballPowerup.height }))
                    {
                        block[i][j].positionPowerup.y = (float)screenHeight;
                        powerup.fireball = true;
                        sounds.powerup = true;
                        timer = countdownTimer;
                    }
                    else if (ball.loss) block[i][j].positionPowerup.y = (float)screenHeight;
                    else
                    {
                        DrawTexture(textures::fireballPowerup, (int)(block[i][j].positionPowerup.x), (int)(block[i][j].positionPowerup.y), WHITE);
                        #if DEBUG 
                        DrawRectangleLines(block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, textures::fireballPowerup.width, textures::fireballPowerup.height, WHITE);
                        #endif
                    }
                }
                else if (block[i][j].kindOfPowerup == Powerups::PADDLE)
                {
                    if (CheckCollisionRecs(player, { block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, (float)textures::paddlePowerup.width, (float)textures::paddlePowerup.height }))
                    {
                        block[i][j].positionPowerup.y = (float)screenHeight;
                        powerup.paddle = true;
                        sounds.powerup = true;
                    }
                    else if (ball.loss) block[i][j].positionPowerup.y = (float)screenHeight;
                    else
                    {
                        DrawTexture(textures::paddlePowerup, (int)(block[i][j].positionPowerup.x), (int)(block[i][j].positionPowerup.y), WHITE);
                        #if DEBUG 
                        DrawRectangleLines(block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, textures::paddlePowerup.width, textures::paddlePowerup.height, WHITE);
                        #endif
                    }
                }
                else
                {
                    if (CheckCollisionRecs(player, { block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, (float)textures::ballPowerup.width, (float)textures::ballPowerup.height }))
                    {
                        block[i][j].positionPowerup.y = (float)screenHeight;
                        powerup.ball = true;
                        sounds.powerup = true;
                    }
                    else if (ball.loss) block[i][j].positionPowerup.y = (float)screenHeight;
                    else
                    {
                        DrawTexture(textures::ballPowerup, (int)(block[i][j].positionPowerup.x), (int)(block[i][j].positionPowerup.y), WHITE);
                        #if DEBUG 
                        DrawRectangleLines(block[i][j].positionPowerup.x, block[i][j].positionPowerup.y, textures::ballPowerup.width, textures::ballPowerup.height, WHITE);
                        #endif
                    }
                }
            }
            else block[i][j].disabledPowerup = true;
        }
    }
}

void drawTimerFireballPowerup(Ball& ball)
{
    int timerBallPosX = ball.position.x - 5;
    int timerBallPosY = ball.position.y - 8;
    int timerPaddlePosX = player.x + textures::blueLongPaddleTexture.width / 2 - 5;
    int timerPaddlePosX2 = player.x + textures::blueShortPaddleTexture.width / 2 - 5;
    int timerPaddlePosX3 = player.x + textures::bluePaddleTexture.width / 2 - 5;
    int timerPaddlePosY = player.y + 5;

    if (powerup.fireball) DrawText(TextFormat("%i", timer / 100), timerBallPosX, timerBallPosY, pos::textsSize4, BLACK);

    if (powerup.fireball && powerup.paddle && powerup.longPaddle)  DrawText(TextFormat("%i", timer / 100), timerPaddlePosX, timerPaddlePosY, pos::textsSize4, BLACK);
    else if (powerup.fireball && powerup.paddle && !powerup.longPaddle)  DrawText(TextFormat("%i", timer / 100), timerPaddlePosX2, timerPaddlePosY, pos::textsSize4, BLACK);
    else if (powerup.fireball)  DrawText(TextFormat("%i", timer / 100), timerPaddlePosX3, timerPaddlePosY, pos::textsSize4, BLACK);
}