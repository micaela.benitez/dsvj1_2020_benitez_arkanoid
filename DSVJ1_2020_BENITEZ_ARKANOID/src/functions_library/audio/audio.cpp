#include "audio.h"

using namespace pong;
using namespace gameplay;

namespace audio
{
	GameMusic gameMusic;
	float musicVolume = 1.0f;

	GameSounds gameSounds;
	float soundVolume = 1.0f;

	void menuAudio()
	{
		UpdateMusicStream(gameMusic.menu);
		
		if (sounds.button) PlaySoundMulti(gameSounds.button);
	}

	void gameplayAudio()
	{
		UpdateMusicStream(gameMusic.gameplay);

		if (sounds.ballCollisionEntities) PlaySoundMulti(gameSounds.ballCollisionEntities);
		if (sounds.ballCollisionWalls) PlaySoundMulti(gameSounds.ballCollisionWalls);
		if (sounds.death) PlaySoundMulti(gameSounds.death);
		if (sounds.powerup) PlaySoundMulti(gameSounds.powerup);
		if (sounds.button) PlaySoundMulti(gameSounds.button);	
	}

	void resultAudio()
	{
		if (sounds.button) PlaySoundMulti(gameSounds.button);
		if (sounds.winner) PlaySoundMulti(gameSounds.winner);
		if (sounds.loser) PlaySoundMulti(gameSounds.loser);
	}

	void loadSounds()
	{
		gameSounds.ballCollisionEntities = LoadSound("res/assets/audio/sounds/ballhit2.wav");
		gameSounds.ballCollisionWalls = LoadSound("res/assets/audio/sounds/ballhit.wav");
		gameSounds.death = LoadSound("res/assets/audio/sounds/death.wav");
		gameSounds.powerup = LoadSound("res/assets/audio/sounds/powerup.wav");
		gameSounds.button = LoadSound("res/assets/audio/sounds/buttons.wav");
		gameSounds.winner = LoadSound("res/assets/audio/sounds/winner.mp3");
		gameSounds.loser = LoadSound("res/assets/audio/sounds/loser.mp3");
	}

	void setSoundsVolume()
	{
		SetSoundVolume(gameSounds.ballCollisionEntities, soundVolume);
		SetSoundVolume(gameSounds.ballCollisionWalls, soundVolume);
		SetSoundVolume(gameSounds.death, soundVolume);
		SetSoundVolume(gameSounds.powerup, soundVolume);
		SetSoundVolume(gameSounds.button, soundVolume);
		SetSoundVolume(gameSounds.winner, soundVolume);
		SetSoundVolume(gameSounds.loser, soundVolume);
	}

	void unloadSounds()
	{
		UnloadSound(gameSounds.ballCollisionEntities);
		UnloadSound(gameSounds.ballCollisionWalls);
		UnloadSound(gameSounds.death);
		UnloadSound(gameSounds.powerup);
		UnloadSound(gameSounds.button);
		UnloadSound(gameSounds.winner);
		UnloadSound(gameSounds.loser);
	}

	void loadMusic()
	{
		gameMusic.menu = LoadMusicStream("res/assets/audio/music/menu.mp3");
		gameMusic.gameplay = LoadMusicStream("res/assets/audio/music/gameplay.mp3");
	}

	void setMusicVolume()
	{
		SetMusicVolume(gameMusic.menu, musicVolume);
		SetMusicVolume(gameMusic.gameplay, musicVolume);
	}

	void unloadMusic()
	{
		UnloadMusicStream(gameMusic.menu);
		UnloadMusicStream(gameMusic.gameplay);
	}
}
