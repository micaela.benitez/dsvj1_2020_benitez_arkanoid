#include "condition_to_win.h"

using namespace pong;
using namespace gameplay;

void checkIfThePlayerWon()
{
	playerWon = true;

	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++)
		{
			if (block[i][j].active && !block[i][j].unbreakable) playerWon = false;
		}
	}
}