#ifndef PADDLE_CONTROLS_H
#define PADDLE_CONTROLS_H

#include "scenes/gameplay/gameplay.h"
#include "functions_library/keys/keys.h"

namespace pong
{
	namespace paddle_controls
	{
		struct Keys
		{
			int leftPaddle = KEY_LEFT;
			int rightPaddle = KEY_RIGHT;

			int leftNumber = 26;
			int rightNumber = 27;
		};
		
		extern Keys key;

		extern Color leftArrowButtonStatus;
		extern Color rightArrowButtonStatus;
		
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif