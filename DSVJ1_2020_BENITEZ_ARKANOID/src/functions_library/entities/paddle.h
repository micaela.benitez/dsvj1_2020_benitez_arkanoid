#ifndef PADDLE_H
#define PADDLE_H

#include "raylib.h"

struct Paddle
{
    Vector2 size;
    Vector2 position;
    float speed;
};

#endif